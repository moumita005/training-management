const {Sequelize, DataTypes} = require('sequelize');


const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST, 
    //  port: env.DATABASE_PORT,
    dialect: 'postgres',
    define: {
      // underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
    // operatorsAliases: false,

    // pool: {
    //   max: 5,
    //   min: 0,
    //   acquire: 30000,
    //   idle: 10000
    // }
  }
);

// const sequelize = new Sequelize(`${process.env.DB_NAME}`, `${process.env.DB_USERNAME}`, `${process.env.DB_PASSWORD}`, {
//   host: process.env.DB_HOST,
//   dialect: 'postgres'
// });

const connectPostgreSQL = () => {
  // console.log(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, process.env.DB_HOST)
  sequelize.authenticate()
  .then(() => console.log('Postgre Database connected...'.cyan.bold))
  .catch(err => console.log('ERROR: ' + err));
};

module.exports = {
  sequelize,
  Sequelize,
  connectPostgreSQL,
  DataTypes
}