const express = require('express');
const dotenv =  require('dotenv');
const morgan = require('morgan');
const colors = require('colors')
const errorHandler = require('./middleware/error')


//Load env vars
dotenv.config({path: './config/config.env'})

const mountRoute = require('./routes/routes')
const {connectPostgreSQL} = require('./config/db')



const app = express();
app.use(express.json());
//dev logging middleware
if(process.env.NODE_ENV === 'development'){
  app.use(morgan('dev'))
}

if (process.env.NODE_ENV !== 'test') {
  connectPostgreSQL()
}

//Mount routers
mountRoute(app) 

app.use(errorHandler)

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log(`server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold.underline))

