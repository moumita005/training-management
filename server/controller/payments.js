const Payment = require('../models/Payment');
const asyncHandler = require('../middleware/async');
const { Enrollment } = require('../models');


exports.getPayments = asyncHandler(async(req,res,next) => {
  const payments = await Payment.findAll({
    include: [Enrollment]
  });

  res.status(200).send({
    success: true,
    count: payments.length,
    message: `show all payments`,
    data: payments
  })
})

exports.getPayment = asyncHandler(async(req,res,next) => {
  const payment = await Payment.findByPk(req.params.id);
  if(!payment){
    res.status(404).send({
      success: false,
      message: `can not find the desired payment id ${req.params.id}`
    })
  }

  res.status(200).send({
    success: true,
    message: ` show the payment of ${req.params.id}`,
    data: payment
  })
})

exports.createPayment = asyncHandler(async(req,res,next) => {
 
  const payment = await Payment.create(req.body);
  
  
  res.status(201).send({
    success: true,
    message: `successfully created payment`,
    data: payment
  })
})

exports.updatedPayment = asyncHandler(async(req,res,next) => {
  const payment = await Payment.update(req.body, {
    where: {_id: req.params.id},
    returning:true
  })
  if(!payment){
    return res.status(404).send({
      success: false,
      message: `can not find desired payment id ${req.params.id}`
    })
  }

  res.status(200).send({
    success: true, 
    message: `successfully updated id ${req.params.id}`,
    data: payment
  })
});


exports.deletePayment =async (req,res,next) => {
  const payment = await Payment.findByPk(req.params.id);
  if(!payment){
    return res.status(404).send({
      success: false,
      message: `can not find desired payment id ${req.params.id}`
    })
  }

  await payment.destroy();

  res.status(200).send({
    success:true,
    message:`successfully deleted ${req.params.id}`,
    data:{}
  })


}