const Enrollment = require('../models/Enrollment');
const asyncHandler = require('../middleware/async');
const {Student, Batch, Payment, Course} = require('../models');

exports.getEnrollments = asyncHandler(async(req,res,next) => {
  const enrollments = await Enrollment.findAll({
    include: [Student, Batch, Payment, Course]
  });
  res.status(200).send({
    success:true,
    count: enrollments.length,
    message: "show all enrolled students",
    data: enrollments
  })
});

exports.getEnrollment = asyncHandler(async(req,res,next) => {
  const enrollment = await Enrollment.findByPk(req.params.id);

  if(!enrollment){
    return res.status(404).send({
      success: false,
      message: `can not find the enrolled student with that id ${req.params.id}`
    })
  }
  res.status(200).send({
    success: true,
    message: `show the enrolled student ${req.params.id}`,
    data: enrollment
  })
})

exports.createEnrollment = asyncHandler(async(req,res,next) => {
  const enrollment = await Enrollment.create(req.body);
  const payment = await Payment.create({ where: { enrollment_id: req.body.enrollment_id , enrolling_date: req.body.enrolling_date, amount: req.body.amount } })
  res.status(201).send({
    success: true,
    message: "created enrollment",
    data: enrollment, payment
  })
})


exports.updateEnrollment = asyncHandler(async(req,res,next) => {
  const enrollment = await Enrollment.update(req.body, {
    where: {_id: req.params.id},
    returning: true
  });

  if(!enrollment){
    return res.status(404).send({
      success: false,
      message: `can not find the enrolled student with id ${req.params.id}`,
    })
  }
  res.status(200).send({
    success: true,
    message: `successfully updated enrolled student ${req.params.id}`,
    data: enrollment
  });
});

exports.deleteEnrollment = asyncHandler(async(req,res,next) => {
  const enrollment = await Enrollment.findByPk(req.params.id);
  if(!enrollment){
    return res.status(404).send({
      success: false,
      message: `can not find the enrolled student with id ${req.params.id}`,
    })
  }
  await enrollment.destroy();
  res.status(200).send({
    success:true,
    message:`successfully deleted desired enrolled id ${req.params.id}`,
    data: {}
  })
})