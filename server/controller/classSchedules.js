const ClassSchedule = require('../models/ClassSchedule')
const asyncHandler = require('../middleware/async');



exports.getSchedules = asyncHandler(async(req,res,next) => {
  const schedules = await ClassSchedule.findAll();

  res.status(200).send({
    success: true,
    count: schedules.length,
    message: "show all class schedules",
    data: schedules
  })
})


exports.getSchedule = asyncHandler(async(req,res,next) => {
  const schedule = await ClassSchedule.findByPk(req.params.id);

  if(!schedule) {
    return res.status(404).send({
      success: false,
      message: `can not find the schedule with the given id ${req.params.id}`
    })
  }

  res.status(200).send({
    success: true,
    message: `show the schedule with id ${req.params.id}`,
    data: schedule
  })
})

exports.createSchedules = asyncHandler(async(req,res,next) => {
  const schedule = await ClassSchedule.create(req.body);

  res.status(201).send({
    success: true,
    message: "successfully created class schedules",
    data: schedule
  })
})

exports.updateSchedule = asyncHandler(async(req,res,next) => {
  const schedule = await ClassSchedule.update(req.body, {
    where: {_id: req.params.id},
    returning: true
  })

  if(!schedule) {
    return res.status(404).send({
      success: false,
      message: `can not find the schedule with the given id ${req.params.id}`
    })
  }

  res.status(200).send({
    success: true,
    message:`updated the schedule ${req.params.id}`,
    data: schedule
  })
})


exports.deleteSchedule =asyncHandler(async (req,res,next) => {
  const schedule = await ClassSchedule.findByPk(req.params.id);
  
  if(!schedule) {
    return res.status(404).send({
      success: false,
      message: `can not find the schedule with the given id ${req.params.id}`
    })
  }

  await schedule.destroy();

  res.status(200).send({
    success: true,
    message: `scheduled deleted of ${req.params.id}`,
    data: {}
  })


})