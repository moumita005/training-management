const asyncHandler = require('../middleware/async');
const { Course, Batch } = require('../models');



exports.getCourses = asyncHandler(async(req,res,next) => {
  const courses = await Course.findAll({
    include: [Batch]
  });

  res.status(200).send({
    success: true,
    count: courses.length,
    data: courses
  })
})


exports.createCourse = asyncHandler( async(req,res,next) => {
  const course = await Course.create(req.body);

  res.status(201).send({
    success: true,
    message: "course has been created",
    data: course
  })
  console.log(course)
})

exports.getCourse = asyncHandler(async(req,res,next) => {
  const course = await Course.findByPk(req.params.id);
  
  if(!course){
    res.status(404).send({
      success:false
    })
  }
  res.status(200).send({
    success: true,
    message: `get single course ${req.params.id}`,
    data: course
  })
})


exports.updateCourse = asyncHandler(async(req,res,next) => {
  const course = await Course.update(req.body, {
    where: {_id: req.params.id},
    returning: true
  })

  if(!course){
    return res.status(404).send({
      success: false,
      message: `can not find the course with ${req.params.id} `
    })
  }
  res.status(200).send({
    success: true,
    message: `updated course with id of ${req.params.id} `,
    data: course
  })
})


exports.deleteCourse = asyncHandler(async(req,res,next) => {
  const course = await Course.findByPk(req.params.id);

  if(!course){
    return res.status(404).send({
      success: false,
      message: `can not find the id ${req.params.id}`
    })
  }

  await course.destroy();

  res.status(200).send({
    success:true,
    message: `successfully deleted the id ${req.params.id}`,
    data: {}
  })

  console.log(course)


})