const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utlis/errorResponse');
const { Student, Enrollment }= require('../models');
const {sequelize} = require('../config/db')


//@desc      Get all Students
//@route     GET /api/students
//@access    Public
exports.getStudents = asyncHandler(async(req,res,next) => {
  
  const students = await Student.findAll({
    include: [Enrollment]
  });
  res.status(200).send({
    success: true,
    count: students.length, 
    message:"Show all students",
    data: students 
  })

  
})


//@desc     Create new Student
//@route    POST /api/students
//@access   Public
exports.createStudent = asyncHandler(async(req,res,next) => {
  let student = await Student.findOne({ where: { email: req.body.email } })
  // console.log(student)

  if(student) {
    return next(new ErrorResponse('email id already exists', 400))
  }
  student = await Student.create(req.body);
  // console.log(student);
  res.status(200).send({
    success: true, 
    message: `Student created with id ${student._id}` ,
    data: student
  })
    
  
})

//@desc      Get single student
//@route     GET /api/students/:id
//@access    Public
exports.getStudent = asyncHandler(async(req,res,next) => {
 
  const student = await Student.findByPk(req.params.id);
  if(!student) {
    return res.status(404).send({
      success: false , 
      message: "404 student not found"
    })
  }
  res.status(200).send({
    success: true, 
    message: `show single student ${req.params.id}`, 
    data: student
  })

  
})

//@desc      Update student
//@route     PUT /api/students/:id
//@access    Public
exports.updateStudent = asyncHandler(async(req,res,next) => {
  
  const student = await Student.update(req.body,{
    where:{ _id : req.params.id },
    returning: true
  } )

  if(!student) return res.status(404).send({
    success:false, 
    message: "404 not found"
  })
  res.status(200).send({
    success:true, 
    message: `update student ${req.params.id}`,
    data: student
  })
  
})


//@desc      Delete Student
//@route     DELETE /api/students/:id
//@access    Public
exports.deleteStudent = asyncHandler(async(req,res,next) => {
 
  const student = await Student.findByPk(req.params.id);
  if(!student){
     return res.status(404).send({
       success: false, 
       message: "404 not found"
      });
  }
  await student.destroy(req.params.id);
  res.status(200).send({
    success: true, 
    message: `delete student ${req.params.id}`, 
    data: {} 
  })
  
})