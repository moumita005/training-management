const Batch = require('../models/Batch');
const ErrorResponse = require('../utlis/errorResponse');
const asyncHandler = require('../middleware/async');
const  {Instructor, Course, Enrollment} = require('../models')
const { v4: uuidv4 } = require('uuid');



exports.getBatches = asyncHandler(async(req,res,next) => {
  const batches = await Batch.findAll({
    include: [Course, Instructor, Enrollment]
  });
  // console.log(JSON.stringify(batches, null, 2))
  res.status(200).send({
    success: true,
    message: "show all batches",
    count: batches.length,
    data: batches
  })
}
)

exports.getBatch = asyncHandler(async(req,res,next) => {
  const batch = await Batch.findByPk(req.params.id);
  if(!batch) {
    return res.status(404).send({
      success: false,
      message: `can not find the batch with the given id ${req.params.id}`
    })
  }

  res.status(200).send({
    success: true,
    message: `Show the single batch ${req.params.id}`,
    data: batch
  })
})

exports.createBatch = asyncHandler(async(req,res,next) => {
  let batch = await Batch.findOne({ where: { batch_no: req.body.batch_no } });
  if(batch){
    // return next(new ErrorResponse('batch_no already exists', 400))
     return res.status(400).send({
      success: false,
      message: "batch_no already exists",
      errors: {batch_no: "batch_no already exists"}
    })
  }
  batch = await Batch.create(req.body);
  
  res.status(201).send({
    success: true,
    message: "successfully created batch",
    data: batch
  })
})


exports.updateBatch = asyncHandler(async(req,res,next) => {
  console.log("htmhjtmjhtht",req.body)
  const batch = await Batch.update(req.body, {
    where: {_id: req.params.id},
    returning: true
  });

  console.log()

  if(!batch){
    return res.status(404).send({
      success:false,
      message: `can not find the batch with the given id ${req.params.id}`
    })
  }

  res.status(200).send({
    success: true,
    message: `updated the with the given id ${req.params.id}`,
    data: batch
  })
})


exports.deleteBatch = asyncHandler(async(req,res,next) => {
  const batch = await Batch.findByPk(req.params.id);

  if(!batch){
    return res.status(404).send({
      success: false,
      message: `can not find the batch with the desired id ${req.params.id}`
    });

  }
  await batch.destroy();

  res.status(200).send({
    success:true,
    message: `deleted the batch with the id ${req.params.id}`,
    data: {}
  })
})