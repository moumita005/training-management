const asyncHandler = require('../middleware/async')
const Instructor  =  require('../models/Instructor')
const { v4: uuidv4 } = require('uuid');
const ErrorResponse = require('../utlis/errorResponse');
const {Batch} = require('../models');

//@desc    Get all instructors
//@route   GET /api/instructors
//@access  Public
exports.getInstructors = asyncHandler(async(req,res,next) => {
  
  const instructors = await Instructor.findAll({
    include: [Batch]
  })


  res.status(200).send({
    success:true, 
    count: instructors.length,
    message: "show all instructors",  
    data: instructors
  })
    
  
});

//@desc    Get single instructors
//@route   GET /api/instructors/:id
//@access  Public
exports.getInstructor =asyncHandler( async(req,res,next) => {

  const instructor = await Instructor.findByPk(req.params.id);

  if(!instructor){
    return res.status(404).send({
      success: false
    })
  }
  res.status(200).send({
    success:true, 
    message: `show single instructors ${req.params.id}`, 
    data: instructor
  })

 
});

//@desc    Create new instructor
//@route   POST /api/instructors
//@access  Public
exports.createInstructor = asyncHandler(async(req,res,next) => {
  const i = await Instructor.findOne({ where: { email: req.body.email } })
  if(i) {
    return next(new ErrorResponse('email id already exists', 400))
  }
  const instructor = await Instructor.create(req.body);
  res.status(201).send({
    success:true, 
    data: instructor
  });

 

});

//@desc    Update single instructor
//@route   PUT /api/instructors/:id
//@access  Public
exports.updateInstructor = asyncHandler(async(req,res,next) => {
  
  const instructor = await Instructor.update(req.body,{
    where:{ _id : req.params.id },
    returning: true
  } )
  if(!instructor) {
    return res.status(404).send({
      success:false, 
      message: "404 not found"
    })
  }
  
  res.status(200).send({
    success:true, 
    message: `update instructor ${req.params.id}`, 
    data: instructor
  })
  
})

//@desc    Delete single instructors
//@route   DELETE /api/instructors/:id
//@access  Public
exports.deleteInstructor = asyncHandler(async(req,res,next) => {
    
  const instructor = await Instructor.findByPk(req.params.id);
  
  if(!instructor) {
    return res.status(404).send({
      success: false, 
      message: '404 not found'
    })
  };
  await instructor.destroy();

  res.status(200).send({
    success:true, 
    message: `delete instructor ${req.params.id}`, 
    data: {}
  })
  
})