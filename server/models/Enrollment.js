const { Sequelize, sequelize } = require('../config/db');
const { v4: uuidv4 } = require('uuid');


const Enrollment = sequelize.define("enrollments", {
  _id: {
    type: Sequelize.UUIDV4,
    primaryKey: true
  },
  batch_id: {
    type: Sequelize.UUIDV4,
    allowNull: false,
    validate: {
      notNull: {msg : "Batch id can not be null"},
      notEmpty: {msg: "Batch id can not be null"}
    }
  },
  student_id: {
    type: Sequelize.UUIDV4,
    allowNull: false,
    validate: {
      notNull: {msg : "Student id can not be null"},
      notEmpty: {msg: "Student id can not be null"}
    }
  },
  course_id: {
    type: Sequelize.UUIDV4,
    allowNull: false,
    validate: {
      notNull: {msg : "Course id can not be null"},
      notEmpty: {msg: "Course id can not be null"}
    }
  },
  enrolling_date: {
    type:Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {msg : "Course id can not be null"},
      notEmpty: {msg: "Course id can not be null"}
    }
  },
  
  enrolling_fee: Sequelize.STRING,
  paid_amount: Sequelize.STRING,
  code: Sequelize.STRING,

})

// Setting UUID
Enrollment.beforeSave( async enrollment => {
  enrollment._id = uuidv4();
});


module.exports = Enrollment;