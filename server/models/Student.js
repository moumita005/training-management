const { Sequelize, sequelize } = require('../config/db');
const { v4: uuidv4 } = require('uuid');


const Student = sequelize.define("students", {
  _id: {
    type: Sequelize.UUIDV4,
    primaryKey: true,
    // defaultValue: uuidv4()
    // defaultValue: Sequelize.UUIDV4
  },
  id:{
    type: Sequelize.INTEGER,
    autoIncrement:true,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    },  
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  father_name: Sequelize.STRING,
  mother_name: Sequelize.STRING,
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {msg: "Email can not be null"},
      isEmail: {msg: "Not a valid email"}
    },
    unique: {
      args:true,
      msg: 'Email address already in use!'
    },

  },
  image: Sequelize.STRING,
  description: Sequelize.STRING,
  phone_no:{
    type: Sequelize.STRING,
    allowNull:false,
    validate: {
      notNull: {msg: "Phone number can not be null"},
      notEmpty: { msg: "Phone can not be empty" },
      // is: {
      //   args: /^(?:\+88|01)?(?:\d{11}|\d{13})$/i,
      //   msg: "Not a vaild phone number"
      // }
    }
  },
  guardian_phone_no: {
    type: Sequelize.STRING,
    allowNull:false,
    validate: {
      notNull: {msg: "Phone number can not be null"},
      notEmpty: { msg: "Phone can not be empty" },
      // is: {
      //   args: /^(?:\+88|01)?(?:\d{11}|\d{13})$/i,
      //   msg: "Not a vaild phone number"
      // }
    }
  },
  guardian_nid_no: Sequelize.STRING,
  present_village: Sequelize.STRING,
  present_post: Sequelize.STRING,
  present_city: Sequelize.STRING,
  present_upazila: Sequelize.STRING,
  present_district: Sequelize.STRING,
  present_division: Sequelize.STRING ,
  permanent_village: Sequelize.STRING,
  permanent_post: Sequelize.STRING,
  permanent_city: Sequelize.STRING,
  permanent_upazila: Sequelize.STRING,
  permanent_district: Sequelize.STRING,
  permanent_division: Sequelize.STRING ,
  student_nid: Sequelize.STRING,
  birthcertificate: Sequelize.STRING,
  gender: Sequelize.STRING,
  date_of_birth: Sequelize.STRING,
  educational_qualification: Sequelize.STRING,
  marital_status: Sequelize.STRING,
  profession: Sequelize.STRING,
  local_guardian_phone_no: {
    type: Sequelize.STRING,
    allowNull:false,
    validate: {
      notNull: {msg: "Phone number can not be null"},
      notEmpty: { msg: "Phone can not be empty" },
      // is: {
      //   args: /^(?:\+88|01)?(?:\d{11}|\d{13})$/i,
      //   msg: "Not a vaild phone number"
      // }
    }
  },
  local_guardian_name: Sequelize.STRING,
  relation_local_guardian: Sequelize.STRING,
  local_guardian_address: Sequelize.STRING,
  local_guardian_nid_no: Sequelize.STRING

});

// Setting UUID
Student.beforeSave( async student => {
  student._id = uuidv4();
});

module.exports = Student;