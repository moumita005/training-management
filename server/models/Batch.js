const { Sequelize, sequelize } = require('../config/db');
const { v4: uuidv4 } = require('uuid');


const Batch = sequelize.define("batches", {
  _id: {
    type: Sequelize.UUIDV4,
    primaryKey: true
  },
  batch_no: {
    type:Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {msg: "Batch no can not be null"},
      notEmpty: {msg: "Batch no can not be empty"}
    },
    unique: {
      args:true,
      msg: 'Email address already in use!'
    },
  },
  // name: {
  //   type: Sequelize.STRING,
  //   allowNull: false,
  //   validate: {
  //     notNull: {msg: "Name can not be null"},
  //     notEmpty: {msg: "Name can not be empty"}
  //   }
  // },
  course_id: {
    type: Sequelize.UUIDV4,
    // allowNull: false,
    // validate: {
    //   notNull: {msg : "Course id can not be null"},
    //   notEmpty: {msg: "Course id can not be null"}
    // }
  },
  instructor_id:{type: Sequelize.UUIDV4} ,
  start_date: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {msg: "Course start date can not be null"},
      notEmpty: {msg : "Course start date can not be null"}
    }
  },
  end_date: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {msg: "Course start date can not be null"},
      notEmpty: {msg : "Course start date can not be null"}
    }
  },
  additional_class: Sequelize.STRING,
  closing: Sequelize.STRING,
  description: Sequelize.STRING,
  status: Sequelize.STRING

} )

// Setting UUID
Batch.beforeSave( async batch => {
  batch._id = uuidv4();
});


module.exports = Batch;