const { Sequelize, sequelize } = require('../config/db');
const { v4: uuidv4 } = require('uuid');


const ClassSchedule = sequelize.define('class_schedules', {
  _id: {
    type: Sequelize.UUIDV4,
    primaryKey: true
  },
  class_day:{
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  class_time:{
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  batch_id : Sequelize.UUIDV4,
});


// Setting UUID
ClassSchedule.beforeSave( async schedule => {
  schedule._id = uuidv4();
});


module.exports = ClassSchedule;