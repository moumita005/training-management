const { Sequelize, sequelize, DataTypes } = require('../config/db');
const { v4: uuidv4 } = require('uuid');


const Payment = sequelize.define("payments", {
  _id: {
    type: Sequelize.UUIDV4,
    primaryKey: true
  },
  enrollment_id: {
    type: DataTypes.UUIDV4,
    // allowNull: false,
    // validate: {
    //   notNull: {msg : "Course id can not be null"},
    //   notEmpty: {msg: "Course id can not be null"}
    // }
  },
  amount: { type: Sequelize.STRING},
  
  paydate: {type: Sequelize.STRING},

})

// Setting UUID
Payment.beforeSave( async payment => {
  payment._id = uuidv4();
});


module.exports = Payment;