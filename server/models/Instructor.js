const { Sequelize, sequelize } = require('../config/db');
const { v4: uuidv4 } = require('uuid');


const Instructor = sequelize.define('instructors', {
  _id: {
    type: Sequelize.UUIDV4,
    primaryKey: true,
    // defaultValue: Sequelize.UUIDV4
  },
  name:{
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: {msg: "Email can not be null"},
      isEmail: {msg: "Not a valid email"}
    },
    unique: {
      args:true,
      msg: 'Email address already in use!'
    },

  },
  designation: Sequelize.STRING,
  course:Sequelize.STRING,  
  description :Sequelize.STRING,
  image: Sequelize.STRING,
  phone_no:{
    type: Sequelize.STRING,
    allowNull:false,
    validate: {
      notNull: {msg: "Phone number can not be null"},
      notEmpty: { msg: "Phone can not be empty" },
      // is: {
      //   args: /^(?:\+88|01)?(?:\d{11}|\d{13})$/i,
      //   msg: "Not a vaild phone number"
      // }
    }
  },
  company_name: Sequelize.STRING,
  address: Sequelize.STRING,
  nid_no: Sequelize.STRING
});


// Setting UUID
Instructor.beforeSave( async instructor => {
  instructor._id = uuidv4();
});


module.exports = Instructor;