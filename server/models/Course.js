const { Sequelize, sequelize } = require('../config/db');
const { v4: uuidv4 } = require('uuid');


const Course = sequelize.define('courses', {
  _id: {
    type: Sequelize.UUIDV4,
    primaryKey: true
  },
  name:{
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  topics:{
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  course_fee:{
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },  
  course_duration: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  description :Sequelize.STRING,
  no_of_classes : Sequelize.STRING,
  discount: Sequelize.STRING
});


// Setting UUID
Course.beforeSave( async course => {
  course._id = uuidv4();
});


module.exports = Course;