const Batch = require('./Batch');
const Course = require('./Course');
const Student = require('./Student');
const Instructor = require('./Instructor')
const Enrollment = require('./Enrollment')
const Payment = require('./Payment')

//relations
Course.hasMany(Batch, {foreignKey: 'course_id'});
Batch.belongsTo(Course, {foreignKey: 'course_id'});

Instructor.hasMany(Batch, {foreignKey: "instructor_id"})
Batch.belongsTo(Instructor, {foreignKey: "instructor_id"})

Student.hasMany(Enrollment, {foreignKey: 'student_id'})
Enrollment.belongsTo(Student, {foreignKey: 'student_id'});

Batch.hasMany(Enrollment, {foreignKey: 'batch_id'});
Enrollment.belongsTo(Batch, {foreignKey: 'batch_id'})

Course.hasMany(Enrollment, {foreignKey: 'course_id'});
Enrollment.belongsTo(Course, {foreignKey: 'course_id'})

Enrollment.hasMany(Payment, {foreignKey: 'enrollment_id'});
Payment.belongsTo(Enrollment, {foreignKey: 'enrollment_id'})

module.exports = {
  Course, 
  Batch,
  Instructor,
  Enrollment,
  Student,
  Payment
}

