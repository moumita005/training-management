const ErrorResponse = require('../utlis/errorResponse')


const errorHandler = ( err , req , res , next ) =>{

  
  let error = {...err}, errors = {};
  error.message = err.message
  return res.send(error)

  //Invalid input id
  if(err.name === 'SequelizeDatabaseError'){
    const message = `Invalid input syntax for type uuid`;
    // errors["_id"] = message
    error = new ErrorResponse(message, 404);
  }

  // Sequelize Validation Error
  if(err.name === 'SequelizeValidationError'){
    err.errors.forEach(error => errors[error.path] = error.message)
    error = new ErrorResponse(errors, 400);
    return res.status(400).json({ success: false,  errors })
  }


  //duplicate key
  if(err.name === 'SequelizeUniqueConstraintError'){
    const message = `duplicate field value entered`
    error = new ErrorResponse(message, 400)
  }

  
  
  res.status(error.statusCode || 500).json({
    success: false,
    message: error.message || 'Server Error',
    errors
  });
}

module.exports = errorHandler