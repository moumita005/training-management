const express = require('express');
const router = express.Router();
const {
  createBatch, 
  getBatches, 
  getBatch, 
  updateBatch,
  deleteBatch
} = require('../controller/batches')

router
  .route('/')
  .get(getBatches)
  .post(createBatch)

router
  .route('/:id')
  .get(getBatch)
  .put(updateBatch)
  .delete(deleteBatch)

module.exports = router