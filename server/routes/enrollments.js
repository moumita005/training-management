const express = require('express');
const router = express.Router();
const { 
  getEnrollments, 
  createEnrollment, 
  getEnrollment,
  updateEnrollment,
  deleteEnrollment
} = require('../controller/enrollments')


router
  .route('/')
  .get(getEnrollments)
  .post(createEnrollment);

router 
  .route('/:id')
  .get(getEnrollment)
  .put(updateEnrollment)
  .delete(deleteEnrollment);


module.exports = router