const instructors = require('./instructors');
const students = require('./students');
const courses = require('./courses')
const batches = require('./batches');
const enrollments = require('./enrollments');
const payments = require('./payments');
const classSchedules = require('./classSchedules')

module.exports = app => {
  app.use('/api/instructors',instructors );
  app.use('/api/students', students );
  app.use('/api/courses', courses);
  app.use('/api/batches', batches);
  app.use('/api/enrollments', enrollments),
  app.use('/api/payments', payments),
  app.use('/api/schedules', classSchedules)
}