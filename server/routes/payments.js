const express = require('express');
const router = express.Router();
const { 
  createPayment, 
  getPayments, 
  getPayment, 
  updatedPayment, 
  deletePayment 
} = require('../controller/payments');


router
  .route('/')
  .get(getPayments)
  .post(createPayment);

router
  .route('/:id')
  .get(getPayment)
  .put(updatedPayment)
  .delete(deletePayment)

module.exports = router;