const express = require('express');
const router = express.Router();
const {
  getInstructor, 
  getInstructors, 
  updateInstructor, 
  createInstructor, 
  deleteInstructor
} = require('../controller/instructors')

router
  .route('/')
  .get(getInstructors)
  .post(createInstructor);

router
  .route('/:id')
  .get(getInstructor)
  .put(updateInstructor)
  .delete(deleteInstructor)

module.exports = router