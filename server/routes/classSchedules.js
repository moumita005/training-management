const express = require('express');
const { createSchedules, getSchedules, getSchedule, updateSchedule, deleteSchedule } = require('../controller/classSchedules');
const router = express.Router();


router
  .route('/')
  .get(getSchedules)
  .post(createSchedules);

router
  .route('/:id')
  .get(getSchedule)
  .put(updateSchedule)
  .delete(deleteSchedule)

module.exports = router