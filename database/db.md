## Prerequisites
```
 CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```


## batches
```sql
CREATE TABLE batches(
  _id uuid default uuid_generate_v4() NOT NULL,
  batch_no VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  course_id uuid default uuid_generate_v4() NOT NULL,
  start_date VARCHAR NOT NULL,
  end_date VARCHAR NOT NULL,
  additional_class VARCHAR,
  closing VARCHAR,
  description VARCHAR,
  status VARCHAR,
  instructor_id default uuid_generate_v4()

  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,

  
  CONSTRAINT batches_pkey PRIMARY KEY (_id));
```


## courses
```sql
CREATE TABLE courses(
  _id uuid default uuid_generate_v4() NOT NULL,
  name VARCHAR NOT NULL,
  topics VARCHAR NOT NULL,
  course_fee VARCHAR NOT NULL,
  course_duration VARCHAR 
  no_of_classes VARCHAR,
  description VARCHAR,
  discount VARCHAR,

  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,

  
  CONSTRAINT courses_pkey PRIMARY KEY (_id));
```


## enrollments
```sql
CREATE TABLE enrollments(
  _id uuid default uuid_generate_v4() NOT NULL,
  student_id uuid default uuid_generate_v4() NOT NULL,
  batch_id uuid default uuid_generate_v4() NOT NULL,
  course_id uuid default uuid_generate_v4() NOT NULL,
  enrolling_date VARCHAR NOT NULL,
  enrolling_fee VARCHAR,
  paid_amount VARCHAR,
  code VARCHAR,

  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,

  
  CONSTRAINT enrollments_pkey PRIMARY KEY (_id));
```

## instructors
```sql
CREATE TABLE instructors(
  _id uuid default uuid_generate_v4() NOT NULL,
  name VARCHAR NOT NULL,
  email VARCHAR NOT NULL,
  designation VARCHAR ,
  course VARCHAR,
  image VARCHAR,
  description VARCHAR,
  phone_no VARCHAR NOT NULL,
  company_name VARCHAR,
  present_village VARCHAR,
  present_post VARCHAR,
  present_city VARCHAR,
  present_state VARCHAR,
  present_district VARCHAR,
  permanent_village VARCHAR,
  permanent_post VARCHAR,
  permanent_city VARCHAR,
  permanent_state VARCHAR,
  permanent_district VARCHAR,
  nid_no VARCHAR,

  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,

  
  CONSTRAINT instructors_pkey PRIMARY KEY (_id));
```

## students
```sql
CREATE TABLE students(
  _id uuid default uuid_generate_v4() NOT NULL,
  id SERIAL NOT NULL,
  name VARCHAR NOT NULL,
  father_name VARCHAR,
  mother_name VARCHAR,
  email VARCHAR NOT NULL,
  image VARCHAR,
  description VARCHAR,
  phone_no VARCHAR NOT NULL,
  guardian_phone_no VARCHAR NOT NULL,
  guardian_nid_no VARCHAR,
  present_village VARCHAR,
  present_post VARCHAR,
  present_city VARCHAR,
  present_upazila VARCHAR,
  present_district VARCHAR,
  present_division VARCHAR,
  permanent_village VARCHAR,
  permanent_post VARCHAR,
  permanent_city VARCHAR,
  permanent_upazila VARCHAR,
  permanent_district VARCHAR,
  permanent_division VARCHAR,
  student_nid VARCHAR,
  birthcertificate VARCHAR,
  gender VARCHAR,
  date_of_birth VARCHAR,
  educational_qualification VARCHAR,
  marital_status VARCHAR,
  profession VARCHAR,
  local_guardian_phone_no VARCHAR NOT NULL,
  relation_local_guardian VARCHAR,
  local_guardian_address VARCHAR,
  local_guardian_nid_no VARCHAR, 
  local_guardian_name VARCHAR, 
  

  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,

  
  CONSTRAINT students_pkey PRIMARY KEY (_id));
```

## payments
```sql
CREATE TABLE payments(
  _id uuid default uuid_generate_v4() NOT NULL,
  enrollment_id uuid default uuid_generate_v4() NOT NULL,
  amount VARCHAR,
  paydate VARCHAR,

  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,

  
  CONSTRAINT payments_pkey PRIMARY KEY (_id));
```

## class_schedules
```sql
CREATE TABLE class_schedules(
  _id uuid default uuid_generate_v4() NOT NULL,
  batch_id uuid default uuid_generate_v4() NOT NULL,
  class_day VARCHAR NOT NULL,
  class_time VARCHAR NOT NULL,

  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,

  
  CONSTRAINT class_schedules_pkey PRIMARY KEY (_id));
```