PGDMP     0    
                x            training_pgs    12.3    12.3     5           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            6           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            7           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            8           1262    32854    training_pgs    DATABASE     �   CREATE DATABASE training_pgs WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United Kingdom.1252' LC_CTYPE = 'English_United Kingdom.1252';
    DROP DATABASE training_pgs;
                postgres    false                        3079    32994 	   uuid-ossp 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
    DROP EXTENSION "uuid-ossp";
                   false            9           0    0    EXTENSION "uuid-ossp"    COMMENT     W   COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';
                        false    2            �            1259    32950    batches    TABLE     �  CREATE TABLE public.batches (
    _id uuid NOT NULL,
    batch_no character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    course_id uuid,
    "from" character varying(255) NOT NULL,
    "to" character varying(255) NOT NULL,
    additional_class character varying(255),
    closing character varying(255),
    description character varying(255),
    status character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.batches;
       public         heap    postgres    false            �            1259    32942    courses    TABLE     �  CREATE TABLE public.courses (
    _id uuid NOT NULL,
    name character varying(255) NOT NULL,
    topics character varying(255) NOT NULL,
    course_fee character varying(255) NOT NULL,
    course_duration character varying(255) NOT NULL,
    no_of_classes character varying(255),
    description character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.courses;
       public         heap    postgres    false            �            1259    32958    enrollments    TABLE     
  CREATE TABLE public.enrollments (
    _id uuid NOT NULL,
    student_id uuid,
    batch_id uuid,
    enrolling_date character varying(255),
    enrolling_fee character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.enrollments;
       public         heap    postgres    false            �            1259    33029    instructors    TABLE     �  CREATE TABLE public.instructors (
    _id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying,
    email character varying,
    designation character varying,
    course character varying,
    image character varying,
    description character varying,
    phone_no character varying,
    company_name character varying,
    address character varying,
    nid_no character varying,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.instructors;
       public         heap    postgres    false    2            �            1259    32966    payments    TABLE     �   CREATE TABLE public.payments (
    _id uuid NOT NULL,
    enrollment_id uuid,
    amount character varying(255),
    paydate character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.payments;
       public         heap    postgres    false            �            1259    32934    students    TABLE     �  CREATE TABLE public.students (
    _id uuid NOT NULL,
    name character varying(255) NOT NULL,
    father_name character varying(255) NOT NULL,
    mother_name character varying(255),
    present_village character varying(255),
    present_post character varying(255),
    present_city character varying(255),
    present_state character varying(255),
    present_district character varying(255),
    permanent_village character varying(255),
    permanent_post character varying(255),
    permanent_city character varying(255),
    permanent_state character varying(255),
    permanent_district character varying(255),
    phone_no character varying(255),
    guardian_phone_no character varying(255),
    guardian_nid_no character varying(255),
    student_nid character varying(255),
    birthcertificate character varying(255),
    email character varying(255) NOT NULL,
    gender character varying(255),
    date_of_birth character varying(255),
    educational_qualification character varying(255),
    marital_status character varying(255),
    profession character varying(255),
    local_guardian_name character varying(255),
    local_guardian_phone_no character varying(255),
    relation_local_guardian character varying(255),
    local_guardian_address character varying(255),
    local_guardian_nid_no character varying(255),
    description character varying(255),
    image character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.students;
       public         heap    postgres    false            /          0    32950    batches 
   TABLE DATA           �   COPY public.batches (_id, batch_no, name, course_id, "from", "to", additional_class, closing, description, status, created_at, updated_at) FROM stdin;
    public          postgres    false    205   %       .          0    32942    courses 
   TABLE DATA           �   COPY public.courses (_id, name, topics, course_fee, course_duration, no_of_classes, description, created_at, updated_at) FROM stdin;
    public          postgres    false    204   M&       0          0    32958    enrollments 
   TABLE DATA           w   COPY public.enrollments (_id, student_id, batch_id, enrolling_date, enrolling_fee, created_at, updated_at) FROM stdin;
    public          postgres    false    206   �&       2          0    33029    instructors 
   TABLE DATA           �   COPY public.instructors (_id, name, email, designation, course, image, description, phone_no, company_name, address, nid_no, created_at, updated_at) FROM stdin;
    public          postgres    false    208   �'       1          0    32966    payments 
   TABLE DATA           _   COPY public.payments (_id, enrollment_id, amount, paydate, created_at, updated_at) FROM stdin;
    public          postgres    false    207   �*       -          0    32934    students 
   TABLE DATA           1  COPY public.students (_id, name, father_name, mother_name, present_village, present_post, present_city, present_state, present_district, permanent_village, permanent_post, permanent_city, permanent_state, permanent_district, phone_no, guardian_phone_no, guardian_nid_no, student_nid, birthcertificate, email, gender, date_of_birth, educational_qualification, marital_status, profession, local_guardian_name, local_guardian_phone_no, relation_local_guardian, local_guardian_address, local_guardian_nid_no, description, image, created_at, updated_at) FROM stdin;
    public          postgres    false    203   )+       �
           2606    32957    batches batches_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.batches
    ADD CONSTRAINT batches_pkey PRIMARY KEY (_id);
 >   ALTER TABLE ONLY public.batches DROP CONSTRAINT batches_pkey;
       public            postgres    false    205            �
           2606    32949    courses courses_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (_id);
 >   ALTER TABLE ONLY public.courses DROP CONSTRAINT courses_pkey;
       public            postgres    false    204            �
           2606    32965    enrollments enrollments_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.enrollments
    ADD CONSTRAINT enrollments_pkey PRIMARY KEY (_id);
 F   ALTER TABLE ONLY public.enrollments DROP CONSTRAINT enrollments_pkey;
       public            postgres    false    206            �
           2606    33037    instructors instructors_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.instructors
    ADD CONSTRAINT instructors_pkey PRIMARY KEY (_id);
 F   ALTER TABLE ONLY public.instructors DROP CONSTRAINT instructors_pkey;
       public            postgres    false    208            �
           2606    32973    payments payments_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (_id);
 @   ALTER TABLE ONLY public.payments DROP CONSTRAINT payments_pkey;
       public            postgres    false    207            �
           2606    32941    students students_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (_id);
 @   ALTER TABLE ONLY public.students DROP CONSTRAINT students_pkey;
       public            postgres    false    203            /   �   x���K�CAE�U�ȼ��4��dR_z�?y�|&�YP�p���!�=�t
��޼@����*�{�����8�*�t�2\ �M����Fʜ��#����22�~"?��7���I6�m�� �@�A�rk1�5�Ð�nH�&Q>*�P�V{(N �r�*�/��j�Q���p,_w,����$���o��lk      .   �   x�}�A
�0��ur��e��d2MrO�M�&�R��VQ��[��oQ,"]!�yY�@���4��R`6u�o�6xs[/�?	"N����121�8fN9�SԯDct��l	cJck,�z��%�"��q~�l���y̢.x��?�����,9B      0   �   x���1B1C��S��|%i�&��%�m�
�Ƀ-K�+*�&1DҬ��p�`G�Z�qc&Ɯ	x
��Dp���G�^O�팆�q��� *B�n��M���B�
��E~e��֥]�^QTi��8�eA�Z�een#��� ����^2}�W��SJ�hQ�      2   �  x���A��6E��S�~@C�H��jN��F��F����s��:	�5�L-�����D($�	����5�k�VH4��������׏����o�~����Rο���r���i}�����V.���������Y4YF��
 	�_P��p�D_��D�tD&LcsR6�C�ѓ�A��mH��p�ö�0�{DHK癔����k'�G`��z�V�ԵTL��{���ޯ����,i^Xg�=�F���T�rh��,�]MBk1v��Ė���9����(M!G%JԪ7�� %Au�D�9)n��<������[~B�]�T����� ���įk��p�-��fc_���u?<'�=�V�*��Gc�T�<kP�n��ֵI���O�P���ϯ��q�]��	1���B����w挏��I��xO�8��Wф��:���c�.�f�'�J7G�����yP� ֑�c9,��$�n�I(Y'�<�V�{������Kit�O�Q�n�	�*��A���x�VRO�5�׷Y���ꬁ�n��C�'8��z�i"�4*�^c#��t�x+iF�{­4y�W�4�t�lc�E��ŗ�$U"�qNY�n���cm
5� g�9�At�Q(�(�A����l�ć[iJR0�װKT��ݣ��l_a{�rȤ������i����$h$��w��5L!7q�ZZ��iyH�?1b�V�j�(yb{�0U�3��2�
��{c��bp��-�'�Jӷy��? <k�<      1   e   x�}ɱ� ����G�	��"\���K�~���Oy��]f�$�-��ՙ���� �[e"C�a�9�@��W�R��iv������}�Z&�      -   �   x���In�0E��)�/hp�@�z�� ɖ��1��9���������ɷ���Am���<hi��Yۜ�ױ�-mZ^����w7�Ģ>�d����WN�T�C��S~|�yZ����t?���@~�֓�^:�x���qsn�%��
9� ԲX�5�/f�d�x~�9����˯��&��1I��v�k�U���s�>�u�     