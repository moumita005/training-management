PGDMP                         x            training_pgs    12.3    12.3     B           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            C           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            D           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            E           1262    32854    training_pgs    DATABASE     �   CREATE DATABASE training_pgs WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United Kingdom.1252' LC_CTYPE = 'English_United Kingdom.1252';
    DROP DATABASE training_pgs;
                postgres    false                        3079    32994 	   uuid-ossp 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
    DROP EXTENSION "uuid-ossp";
                   false            F           0    0    EXTENSION "uuid-ossp"    COMMENT     W   COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';
                        false    2            �            1259    32950    batches    TABLE     �  CREATE TABLE public.batches (
    _id uuid NOT NULL,
    batch_no character varying(255) NOT NULL,
    course_id uuid,
    start_date character varying(255) NOT NULL,
    end_date character varying(255) NOT NULL,
    additional_class character varying(255),
    closing character varying(255),
    description character varying(255),
    status character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    instructor_id uuid
);
    DROP TABLE public.batches;
       public         heap    postgres    false            �            1259    41046    class_schedules    TABLE     �   CREATE TABLE public.class_schedules (
    _id uuid NOT NULL,
    class_day character varying NOT NULL,
    class_time character varying NOT NULL,
    batch_id uuid,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
 #   DROP TABLE public.class_schedules;
       public         heap    postgres    false            �            1259    32942    courses    TABLE     �  CREATE TABLE public.courses (
    _id uuid NOT NULL,
    name character varying(255) NOT NULL,
    topics character varying(255) NOT NULL,
    course_fee character varying(255) NOT NULL,
    course_duration character varying(255) NOT NULL,
    no_of_classes character varying(255),
    description character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    discount character varying(255)
);
    DROP TABLE public.courses;
       public         heap    postgres    false            �            1259    32958    enrollments    TABLE     }  CREATE TABLE public.enrollments (
    _id uuid NOT NULL,
    student_id uuid NOT NULL,
    batch_id uuid NOT NULL,
    enrolling_date character varying(255),
    enrolling_fee character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    course_id uuid NOT NULL,
    paid_amount character varying(255),
    code character varying
);
    DROP TABLE public.enrollments;
       public         heap    postgres    false            �            1259    33029    instructors    TABLE       CREATE TABLE public.instructors (
    _id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    designation character varying,
    course character varying,
    image character varying,
    description character varying,
    phone_no character varying NOT NULL,
    company_name character varying,
    address character varying,
    nid_no character varying,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.instructors;
       public         heap    postgres    false    2            �            1259    32966    payments    TABLE     �   CREATE TABLE public.payments (
    _id uuid NOT NULL,
    enrollment_id uuid,
    amount character varying(255),
    paydate character varying(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
    DROP TABLE public.payments;
       public         heap    postgres    false            �            1259    41067    students    TABLE     �  CREATE TABLE public.students (
    _id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying,
    father_name character varying,
    mother_name character varying,
    email character varying,
    image character varying,
    description character varying,
    phone_no character varying,
    guardian_phone_no character varying,
    guardian_nid_no character varying,
    present_village character varying,
    present_post character varying,
    present_city character varying,
    present_upazila character varying,
    present_district character varying,
    present_division character varying,
    permanent_village character varying,
    permanent_post character varying,
    permanent_city character varying,
    permanent_upazila character varying,
    permanent_district character varying,
    permanent_division character varying,
    student_nid character varying,
    birthcertificate character varying,
    gender character varying,
    date_of_birth character varying,
    educational_qualification character varying,
    marital_status character varying,
    profession character varying,
    local_guardian_phone_no character varying,
    relation_local_guardian character varying,
    local_guardian_address character varying,
    local_guardian_nid_no character varying,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    local_guardian_name character varying,
    id integer NOT NULL
);
    DROP TABLE public.students;
       public         heap    postgres    false    2            �            1259    49238    students_id_seq    SEQUENCE     �   CREATE SEQUENCE public.students_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.students_id_seq;
       public          postgres    false    209            G           0    0    students_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.students_id_seq OWNED BY public.students.id;
          public          postgres    false    210            �
           2604    49240    students id    DEFAULT     j   ALTER TABLE ONLY public.students ALTER COLUMN id SET DEFAULT nextval('public.students_id_seq'::regclass);
 :   ALTER TABLE public.students ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    209            9          0    32950    batches 
   TABLE DATA           �   COPY public.batches (_id, batch_no, course_id, start_date, end_date, additional_class, closing, description, status, created_at, updated_at, instructor_id) FROM stdin;
    public          postgres    false    204   �.       =          0    41046    class_schedules 
   TABLE DATA           g   COPY public.class_schedules (_id, class_day, class_time, batch_id, created_at, updated_at) FROM stdin;
    public          postgres    false    208   �0       8          0    32942    courses 
   TABLE DATA           �   COPY public.courses (_id, name, topics, course_fee, course_duration, no_of_classes, description, created_at, updated_at, discount) FROM stdin;
    public          postgres    false    203   21       :          0    32958    enrollments 
   TABLE DATA           �   COPY public.enrollments (_id, student_id, batch_id, enrolling_date, enrolling_fee, created_at, updated_at, course_id, paid_amount, code) FROM stdin;
    public          postgres    false    205   2       <          0    33029    instructors 
   TABLE DATA           �   COPY public.instructors (_id, name, email, designation, course, image, description, phone_no, company_name, address, nid_no, created_at, updated_at) FROM stdin;
    public          postgres    false    207   K3       ;          0    32966    payments 
   TABLE DATA           _   COPY public.payments (_id, enrollment_id, amount, paydate, created_at, updated_at) FROM stdin;
    public          postgres    false    206   �4       >          0    41067    students 
   TABLE DATA           _  COPY public.students (_id, name, father_name, mother_name, email, image, description, phone_no, guardian_phone_no, guardian_nid_no, present_village, present_post, present_city, present_upazila, present_district, present_division, permanent_village, permanent_post, permanent_city, permanent_upazila, permanent_district, permanent_division, student_nid, birthcertificate, gender, date_of_birth, educational_qualification, marital_status, profession, local_guardian_phone_no, relation_local_guardian, local_guardian_address, local_guardian_nid_no, created_at, updated_at, local_guardian_name, id) FROM stdin;
    public          postgres    false    209   �5       H           0    0    students_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.students_id_seq', 3, true);
          public          postgres    false    210            �
           2606    32957    batches batches_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.batches
    ADD CONSTRAINT batches_pkey PRIMARY KEY (_id);
 >   ALTER TABLE ONLY public.batches DROP CONSTRAINT batches_pkey;
       public            postgres    false    204            �
           2606    41053 $   class_schedules class_schedules_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.class_schedules
    ADD CONSTRAINT class_schedules_pkey PRIMARY KEY (_id);
 N   ALTER TABLE ONLY public.class_schedules DROP CONSTRAINT class_schedules_pkey;
       public            postgres    false    208            �
           2606    32949    courses courses_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (_id);
 >   ALTER TABLE ONLY public.courses DROP CONSTRAINT courses_pkey;
       public            postgres    false    203            �
           2606    32965    enrollments enrollments_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.enrollments
    ADD CONSTRAINT enrollments_pkey PRIMARY KEY (_id);
 F   ALTER TABLE ONLY public.enrollments DROP CONSTRAINT enrollments_pkey;
       public            postgres    false    205            �
           2606    33037    instructors instructors_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.instructors
    ADD CONSTRAINT instructors_pkey PRIMARY KEY (_id);
 F   ALTER TABLE ONLY public.instructors DROP CONSTRAINT instructors_pkey;
       public            postgres    false    207            �
           2606    32973    payments payments_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (_id);
 @   ALTER TABLE ONLY public.payments DROP CONSTRAINT payments_pkey;
       public            postgres    false    206            �
           2606    41075    students students_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (_id);
 @   ALTER TABLE ONLY public.students DROP CONSTRAINT students_pkey;
       public            postgres    false    209            9   '  x���Kn�0D��Sd?���(�9A6�P�?°��L2�I/	���WdI�Y�S�� ;���V��"<�:�yk��
��ҞP���V��A̅�����ddb`!�D.�S����?��Q�� �mP�#1c�XV�.���#�!4��<�\1ǘeX=��-(�D|(S��Hg�<��e@���q�[��L��B�uoUV�G�1�<�𭼗vC%�Ndح�hMd�8�{��B~�]\O��V/+'�{B�Eq��5!��Їo��c��\n{�MYYHS������,���� ���et��T����'w㮰�f�V�e��]9LT��7o�����^�j+�n�ct�DG�t,�[������a(�M2r�g�JF�yj�OHτ���f`�;52I�ǀ��5ژ��X�R.�{����V�:ę��V(�<s��b��2��MO��������z������D���9뿒L�C����as�Іv��{<������)���[��h�R���H�Yc�/����-��Bډ�?����v�n�߆�{o      =   Y   x�}�1
�0 �9y��D����G��.iuTq�������3��+Eߔ����� ��L��X�=w((+�(I�$�)|c��~
ˀ�/�J      8   �   x�}�;NC1���w��Bc�ˏ�"XA۱UP����KT����'M�y�
P@	+XD�i13�I�����2�������%Dt�|v�O����u',��Wɏ�;>l5$�4��+B��!é*�o�Ο�ګ����/�K ���p*}�@sA���0bkG	*����T�)�I�֎��rG�����X(y&���>]��߶�9�U�      :   (  x���KnA���"{��ힳ�q��L��B �.�\_s�Qw�u�7�8��B�jsI�5���e��6�2Av?��D8R���s����[@�1�bYQ�{^|#��������QzX���OR;�gi�
(aBwd8u�.rh\�����H�
k� �`���-�d��BIR�˭�� ���3���"�\n�v��H�W<Ė={��1�'�F���3ݯJZ�����`"�	!�;UI��������Ϫ�j1-�if�������w�P�$_��A~������	��~�^? (}�B      <   �  x���K�� ���)���xT��jN�\`6/{Ҷ[��Q�~p�b�%J$DA� ���!�4.\�9`O�B�<�{I�Wl^��_]��Ζ�u��%��Y�A�
�����ܺmao���]Bf���-e�,����cH��B��  ��2Y5gSB	.W��f�È���	���9eQY�т����!�Ё
�A����B࿃O�����o�eeBvh��@���?b�]`P��8�0����>�Q� �>�ӽ6���YV9n{�\��	��	�_��U}����{���m[�W��;Ѝ G	�\�N~�P���E��p��������E.�r"8"�i��5ў�\nS�Cy�S,mNWJ�U��D56�J�6T�S���\7S��M��x�Vv�Ӵ2*9e*iz�$��T����&�H���t:����j      ;   �   x�}λ1D�خ������ZH��	l��D4���m9���ac��O��,�%�y�@�Ka�/t?m7w]�QX��@�R���҃�E�"9���O�c�{�	�-�e��k�Y�GꓷE@�f�,��Ne�k���"��<�ů�;��Qk� �C=Y      >   �  x���Kn�0���)�/(p����	z�lF"e�+�"=}I[��:IwHp�?8�u`lͭ��+G��@�{��`-J�k2��bđL.l�}������Pu�H�~�������C]�9c�y^������.�R�6��1���{ͽ�qF�S.J0����0���S��P)^�T+�Nc�����JS[ڪ�R8 p�z�XMָY7E
��xr!�{\ WRP��3�ˣW��]X�5+w>��g[�0Y֯1�.��W��y���]'�{�w/�ֿ�w
ٹj\���"A(MF�JD.qv�0�Rʗ����Z�\K�{��RiHɥJ�?�aJ����U����cj�;�q�`=Nt�1��&�K*�*�`��Tv�8����/l۷*��??0R��(%<�%���� չ/9ϵ�ևx�9���OǄ�3f���򧏧򌠼" r(�ڐT���m������*A��w�S�&�x����sIJ�     