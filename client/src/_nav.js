import {
  AppstoreOutlined,
  BarChartOutlined,
  CloudOutlined,
  ShopOutlined,
  TeamOutlined,
  UserOutlined,
  UploadOutlined,
  VideoCameraOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  DashboardOutlined
} from '@ant-design/icons';

export default {
  items: [
    {
      name: "DASHBOARD",
      url: "/dashboard",
      icon: <DashboardOutlined />
    },
    {
      title: "Student",
      icon:<TeamOutlined/>,
      children: [
        {
          name: "Add Student",
          url: "/student/new",
          icon: "far fa-user",
        },
        {
          name: "Students",
          url: "/students",
          icon: "fas fa-user-friends"
        }
      ]
    },
    {
      title: "Instructor",
      icon: {UserOutlined},
      children: [
        {
          name: "Add Instructor",
          url: "/instructor/new",
          icon: "far fa-user"
        },
        {
          name: "Instructors",
          url: "/instructors",
          icon: "fas fa-user-friends"
        }
      ]
    },
    {
      title: "Course",
      icon:<TeamOutlined/>,
      children: [
        {
          name: "Add Course",
          url: "/course/new",
          icon: "far fa-user",
        },
        {
          name: "Courses",
          url: "/courses",
          icon: "fas fa-user-friends"
        }
      ]
    },
    {
      title: "Batch",
      icon:<TeamOutlined/>,
      children: [
        {
          name: "Add Batch",
          url: "/batch/new",
          icon: "far fa-user",
        },
        {
          name: "Batches",
          url: "/batches",
          icon: "fas fa-user-friends"
        }
      ]
    },
    {
      title: "Enrollment",
      icon:<UserOutlined/>,
      children: [
        {
          name: "Add Enrollment",
          url: "/enrollment/new",
          icon: "far fa-user",
        },
        {
          name: "Enrollments",
          url: "/enrollments",
          icon: "fas fa-user-friends"
        }
      ]
    },
    {
      title: "Payment",
      icon:<UserOutlined/>,
      children: [
        {
          name: "Add Payment",
          url: "/payment/new",
          icon: "far fa-user",
        },
        {
          name: "Payments",
          url: "/payments",
          icon: "fas fa-user-friends"
        }
      ]
    },
  ]
}