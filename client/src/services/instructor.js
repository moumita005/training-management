import http from './http';


const apiUrl = '/api/instructors';


export function getInstructors(){
  return http.get(apiUrl);
}

export function getInstructor(id){
  return http.get(`${apiUrl}/${id}`)
}

export function saveInstructor(data){
  if(data._id) return http.put(`${apiUrl}/${data._id}`,data)
  return http.post(apiUrl, data)
}

export function deleteInstructor(id){
  return http.delete(`${apiUrl}/${id}`)
}