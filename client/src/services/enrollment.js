import http from './http';

const apiUrl = '/api/enrollments'

export function getEnrollments(){
  return  http.get(apiUrl)
}


export function getEnrollment(id){
  return http.get(`${apiUrl}/${id}`)
}


export function saveEnrollment(data){
  if(data._id) return http.put(`${apiUrl}/${data._id}`,data);
  return http.post(apiUrl, data)
}


export function deleteEnrollment(id){
  return http.delete(`${apiUrl}/${id}`)
}