import http from './http';


const apiUrl = '/api/payments';


export function getPayments(){
  return http.get(apiUrl);
}

export function getPayment(id){
  return http.get(`${apiUrl}/${id}`)
}

export function savePayment(data){
  if(data._id) return http.put(`${apiUrl}/${data._id}`,data)
  return http.post(apiUrl, data)
}

export function deletePayment(id){
  return http.delete(`${apiUrl}/${id}`)
}