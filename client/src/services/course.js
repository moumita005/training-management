import http from './http';

const apiUrl = '/api/courses';


export function getCourses(){
  return http.get(apiUrl)
}

export function getCourse(id){
  return http.get(`${apiUrl}/${id}`)
}

export function saveCourse(data){
  if(data._id) return http.put(`${apiUrl}/${data._id}`, data);
  return http.post(apiUrl, data)
}

export function deleteCourse(id){
  return http.delete(`${apiUrl}/${id}`)
}