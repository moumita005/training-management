import http from './http';

const apiUrl = "/api/students";

export function getStudents(){
  return http.get(apiUrl);
}

export function getStudent(id){
  return http.get(`${apiUrl}/${id}`)
}

export function saveStudent(data){
  if(data._id) return http.put(`${apiUrl}/${data._id}`, data)
  return http.post(apiUrl, data);
}

export function deleteStudent(id){
  return http.delete(`${apiUrl}/${id}`)
}