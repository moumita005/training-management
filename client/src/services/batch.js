import http from './http';
import { getInstructor } from './instructor';

const apiUrl = '/api/batches';


export function getBatches(){
  return http.get(apiUrl)
}


export function getBatch(id){
  return http.get(`${apiUrl}/${id}`)
}

export function saveBatch(data){
  if(data._id) return http.put(`${apiUrl}/${data._id}`, data)
  return http.post(apiUrl, data);
}


export function deleteBatch(id){
  return http.delete(`${apiUrl}/${id}`)
}