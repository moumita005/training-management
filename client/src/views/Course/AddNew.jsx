import React, { Component } from 'react'
import {  getCourse, saveCourse } from '../../services/course';

export default class AddNewCourse extends Component {
  state={
    data:{}
  }
  
  async componentDidMount(){
    const course_id = this.props.match.params.id;

    if(course_id === 'new') return;
    const {data:{data}} = await getCourse(course_id)
    this.setState({data})
  }

  handleChange = (e) => {
    const data = {...this.state.data};
    data[e.target.name] = e.target.value;
    this.setState({data})
  }

  handleSubmit = async(e) => {
    e.preventDefault();
    const data = {...this.state.data};
    console.log(data)
    await saveCourse(data);
    this.setState({data: {}})
  }

  render() {
    const {data} = this.state
    return (
      <div>
        <div className="container p-lg-5  bg-white">
          <div className="row">
            <div className="col-md-12">
              <h2 className="text-center lead my-5"><span style={{borderBottom: "2px solid grey", color: '#616161!important'}}>Course Information</span></h2>
              <form action="" onSubmit={this.handleSubmit}>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Name :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="name"
                      value= {data.name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Topics :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="topics"
                      value= {data.topics || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Course Fee :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="course_fee"
                      value= {data.course_fee || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Course Duration :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="course_duration"
                      value= {data.course_duration || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                  
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>No of Classes :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="no_of_classes"
                      value= {data.no_of_classes || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Discount:</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="discount"
                      value= {data.discount || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Description:</p>
                  </div>
                  <div className="col-md-10">
                    <textarea 
                      name="description"
                      className="form-control" 
                      onChange= {this.handleChange}
                      rows="3"
                    ></textarea>
                  </div>
                </div>
                <button className="btn btn-outline-dark my-5 float-right" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
