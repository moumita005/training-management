import React, { Component } from 'react';
import {toast} from 'react-toastify'
import { getBatch, saveBatch } from '../../services/batch';
import { getCourses } from '../../services/course';
import {  getInstructors } from '../../services/instructor';

export default class AddNewBatch extends Component {
  state={
    data: {},
    errors:{},
    instructors: [],
    courses: []
  }

  async componentDidMount(){
    const batch_id = this.props.match.params.id;
    const instructor = await getInstructors()
    const course = await getCourses()
    this.setState({instructors:instructor.data.data, courses: course.data.data})
    if(batch_id === 'new') return
    const batch = await getBatch(batch_id);
    console.log(batch)
    
    
    this.setState({ data: batch.data.data})
   
  }

  handleChange = (e) => {
    const data = {...this.state.data};
    const errors = {...this.state.errors};
    data[e.target.name] = e.target.value;
    // delete errors[e.target.name];
 
    this.setState({data, errors})
  }

  handleSubmit = async(e) => {
    e.preventDefault();
    const {data} = this.state;
    console.log(data)
    try{
      await saveBatch(data);
      this.setState({ data: {} })

    }
    catch(err){
      if (err.response && err.response.status === 400) {
        console.log(err.response.data.errors)
        this.setState({ errors: err.response.data.errors });
      }
    }
  }

  

  render() {
    const {data,instructors,errors} = this.state
    return (
      <div>
        <div className="container p-lg-5  bg-white">
          <h3 className="text-center my-5 lead"><span style={{borderBottom: "2px solid grey", color: '#616161!important'}}>Batch Information</span></h3>
          <div className="row">
            <div className="col-md-12">
              <form action="" onSubmit={this.handleSubmit}>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span>Course :</p>
                  </div>
                  <div className="col-md-10">
                    <select 
                        className="form-select"
                        name="course_id"
                        value= {data.course_id || ''}
                        onChange={this.handleChange}
                        required
                      >
                        <option>Select Course</option>

                        {
                          this.state.courses.map((course)=>{
                            
                            return <option key={course._id} value={course._id}>{course.name}</option>
                          })
                        }
                        
                        
                        
                      </select>
                    {/* <input 
                      name="course_id"
                      value= {data.course_id || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    /> */}
                  </div>
                </div>
              
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Batch no :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="batch_no"
                      value= {data.batch_no || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                    <span className="text-danger">{errors.batch_no}</span>
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Instructor :</p>
                  </div>
                  <div className="col-md-10">
                    
                    <select 
                      className="form-select"
                      name="instructor_id"                      
                      value= {data.instructor_id || ''}
                      onChange={this.handleChange}
                    >
                      <option>Select Instructor</option>

                      {
                        instructors.map((instructor)=>{
                          return <option key={instructor._id} value={instructor._id}>{instructor.name}</option>
                        })
                      }
                      
                      
                      
                    </select>
                    {/* <input 
                      name="instructor_id"
                      value= {data.instructor_id || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                  
                    /> */}
                  </div>
                </div>
                {/* <div className="row">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Name :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="name"
                      value= {data.name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div> */}
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Start Date :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      type="date"
                      name="start_date"
                      value= {data.start_date || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> End Date :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      type="date"
                      name="end_date"
                      value= {data.end_date || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div>
                
                
                
                {/* <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Discount:</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="discount"
                      value= {data.discount || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div> */}
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Description:</p>
                  </div>
                  <div className="col-md-10">
                    <textarea 
                      name="description"
                      className="form-control" 
                      onChange= {this.handleChange}
                      rows="3"
                    ></textarea>
                  </div>
                </div>
                <button className="btn btn-outline-dark my-5 float-right" type="submit">Submit</button>
              </form>
            </div>
            {/* <div className="col-md-6">
              <div className="container-fluid">
                <div className="row">
                  
                  <div className="col-md-2">
                    <p>Name:</p>
                  </div>
                  <div className="col-md-10">
                    {instructors.map(instructor=>{
                      return 
                    })}
                  </div>
                </div>
              </div>
            </div> */}
          </div>
        </div>
      </div>
    )
  }
}
