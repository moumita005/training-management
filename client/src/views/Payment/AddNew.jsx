import React, { Component } from 'react'
import { getEnrollments } from '../../services/enrollment';
import { savePayment } from '../../services/payment';

export default class AddNewPayment extends Component {
  state={
    data: {},
    enrollments:[]
  }
   
  async componentDidMount(){
    const { data: { data: enrollments } } = await  getEnrollments();
    this.setState({ enrollments })
  }


  handleChange = (e) =>{
    const data = {...this.state.data};
    data[e.target.name] = e.target.value;
    this.setState({ data })

  }

  handleSubmit = async(e) =>{
    e.preventDefault();
    const {data} = this.state;
    console.log(data)
    await savePayment(data);
    this.setState({ data : {} })
  }

  render() {
    const { data , enrollments } = this.state
    return (
      <div>
        <div className="container p-lg-5  bg-white">
          <h3 className="text-center my-5 lead"><span style={{borderBottom: "2px solid grey", color: '#616161!important'}}>Payment Information</span></h3>
          <div className="row">
            <div className="col-md-12">
              <form action="" onSubmit={this.handleSubmit}>
              <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Enrollment :</p>
                  </div>
                  <div className="col-md-10">
                    <select
                      className="form-select"
                      name="enrollment_id"
                      value= {data.enrollment_id || ''}
                      onChange={this.handleChange}
                      required
                    >
                    <option>Select Enrollment Code</option>
          
                    {
                      enrollments.map(enrollment=>{
                        // console.log(enrollment.code)
                        return(
                          <option key={enrollment._id} value={enrollment._id}> {enrollment.code} </option>
                        )
                      })
                    }
                    
                    </select>
                  </div>
                </div>
                
                
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Amount :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="amount"
                      value= {data.amount || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                  
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Pay Date :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="paydate"
                      value= {data.paydate || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                          
                    />
                  </div>
                </div>
                
                <button className="btn btn-outline-dark my-5 float-right" type="submit">Submit</button>
              </form>
            </div>
            
          </div>
        </div>
      </div>
    )
  }
}
