import React, { Component } from 'react'
import { getBatches } from '../../services/batch';
import { getCourses } from '../../services/course';
import { getEnrollment, saveEnrollment } from '../../services/enrollment';
import { getStudents } from '../../services/student';

export default class AddNewEnrollment extends Component {
  state={
    data: {},
    errors: {},
    students: [],
    courses: [],
    batches: []
  }

  async componentDidMount(){
    const enrollment_id = this.props.match.params.id;
    const {data:{data:students}}  = await getStudents();
    const {data:{data:courses}} = await getCourses()
    const {data:{data:batches}} = await getBatches();
    this.setState({students, courses, batches})

    if(enrollment_id ==='new') return;
    const {data: {data}} = await getEnrollment(enrollment_id)
    console.log(data.data)
    this.setState({data})
    
  }
  
  handleChange = ({ target }) => {
    const data = {...this.state.data};
    const errors = {...this.state.errors};
    data[target.name] = target.value;
    delete errors[target.name];
    if(target.name==="course_id") delete data.batch_id;
    if(data.student_id && data.batch_id){
      const student = this.state.students.find(student=> student._id === data.student_id);
      const batch = this.state.batches.find(batch=> batch._id === data.batch_id);
      console.log(student,batch)
      data.code = student.id + '-'+ batch.batch_no
    }
    this.setState({ data, errors });
  }

  handleSubmit = async e =>{
    e.preventDefault();
    const {data} = this.state;
    console.log(data);
    // try{
      await saveEnrollment(data);
      this.setState({ data : {} })
    // }
    // catch(err){
    //   if(err.response && err.response.status===400){
    //     console.log(err)
    //     // this.setState({errors: err.response.data.errors})
    //   }
    // }
  }

  render() {
    const { data, errors, students, courses, batches } = this.state;
    
    
    return (
      <div>
        <div className="container p-lg-5  bg-white">
          <h3 className="text-center my-5 lead"><span style={{borderBottom: "2px solid grey", color: '#616161!important'}}>Enrollment Information</span></h3>
          <div className="row">
            <div className="col-md-12">
              <form action="" onSubmit={this.handleSubmit}>
              <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Student :</p>
                  </div>
                  <div className="col-md-10">
                    <select
                      className="form-select"
                      name="student_id"
                      value= {data.student_id || ''}
                      onChange={this.handleChange}
                      required
                    >
                    <option>Select Student</option>

                    {
                      students.map((student)=>{
                        return(
                          <option key={student._id} value={student._id}>{student.name}</option>
                        )
                      })
                    }
                    </select>
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span>Course :</p>
                  </div>
                  <div className="col-md-10">
                    <select 
                      className="form-select"
                      name="course_id"
                      value= {data.course_id || ''}
                      onChange={this.handleChange}
                      required
                      >
                        <option>Select Course</option>

                        {
                          courses.map((course)=>{
                            return <option key={course._id} value={course._id}>{course.name}</option>
                          })
                        }
                      </select>
                      <span className="text-danger">{errors.course_id}</span>
                  </div>
                </div>
              
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Batch :</p>
                  </div>
                  <div className="col-md-10">
                    <select 
                      className="form-select"
                      name="batch_id"
                      value= {data.batch_id || ''}
                      onChange={this.handleChange}
                      required 
                    >
                      <option>Select Batch</option>
                    {                       
                      batches.filter(b=>b.course_id===data.course_id).map((batch)=>{
                        return(
                          <option key={batch._id} value={batch._id}>{batch.batch_no}</option>
                        )
                      })
                    }
                    </select>
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Code :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="code"
                      value= {data.code || ''}
                      className="form-control form-control-sm " 
                      // onChange={this.handleChange}
                      // required                  
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Enrolling Fee :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="enrolling_fee"
                      value= {data.enrolling_fee || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                          
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Enrolling Date :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="enrolling_date"
                      value= {data.enrolling_date || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                  
                    />
                  </div>
                </div>
                
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p> Admission Fee :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="paid_amount"
                      value= {data.paid_amount || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                          
                    />
                  </div>
                </div>
                <button className="btn btn-outline-dark my-5 float-right" type="submit">Submit</button>
              </form>
            </div>
            
          </div>
        </div>
      </div>
    )
  }
}
