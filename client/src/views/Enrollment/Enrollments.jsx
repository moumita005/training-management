import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { Modal } from 'antd'
import { deleteEnrollment, getEnrollment, getEnrollments } from '../../services/enrollment';


const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };
  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

export default class Enrollments extends Component {
  state={
    data: {},
    enrollments: [],
    deleteShow: false,
    viewShow:false,
    rowsPerPage:10,
    page:0

  }

  async componentDidMount(){
  
      const { data:{ data : enrollments } } = await getEnrollments();
      console.log(enrollments);
      this.setState({enrollments: enrollments})
    
    
  }
   
  handleViewShow = (data) => {
    this.setState({data, viewShow: true})
  }
 
   handleView = async () => {
     const {data} = this.state
     const enrollment = await getEnrollment(data._id)
     this.setState({data: enrollment.data.data, viewShow:false})
     
    //  console.log(enrollment.data.data)
  }

  handleClose=() =>{
    this.setState({data:{}, deleteShow:false, viewShow:false})
  }
 
  handleDeleteShow = (data) => {
    this.setState({data, deleteShow: true})
  }

  handleDelete = async(e) => {
    e.preventDefault();
    const {data} = this.state;
    await deleteEnrollment(data._id);
    console.log(data._id)
    const enrollments = this.state.enrollments.filter(enrollment=> enrollment._id !== data._id);
    this.setState({enrollments, deleteShow: false})


  }

  handleChangePage =(e, newPage) => {
    console.log("before",newPage)
    this.setState({page:newPage})
    console.log("after",newPage)
  }

  handleChangeRowsPerPage = (e) => {
  
    this.setState({rowsPerPage:e.target.value,page:0})
    
  }

  render() {
    const {enrollments,page,rowsPerPage,deleteShow, viewShow,data} = this.state
    return (
      <div>
        <h2 className="text-center my-4">Enrollment Details</h2>
        <TableContainer  component={Paper}>
          <Table  aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell className="font-weight-bold" align="left">Name</TableCell>
                <TableCell className="font-weight-bold" align="left">Course</TableCell>
                <TableCell className="font-weight-bold" align="left">Batch no</TableCell>
                <TableCell className="font-weight-bold" align="left">Code</TableCell>
                <TableCell className="font-weight-bold" align="left">enrollment Date</TableCell>
                <TableCell className="font-weight-bold" align="left">Enrolling Fee</TableCell>
                <TableCell className="font-weight-bold" align="left">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                  ? enrollments.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : enrollments
                ).map((enrollment,index) => (
                <TableRow key={index}>
                  <TableCell >{enrollment.student && enrollment.student.name} </TableCell>
                  <TableCell align="left">{enrollment.course.name}</TableCell>
                  <TableCell align="left">{enrollment.batch.batch_no}</TableCell>
                  <TableCell align="left">{enrollment.code}</TableCell>
                  <TableCell align="left">{enrollment.enrolling_date}</TableCell>
                  <TableCell align="left">{enrollment.enrolling_fee}</TableCell>
                  <TableCell align="left">
                    {/* <i 
                      className="fa fa-eye mr-3 text-primary" 
                      onClick={()=>this.handleViewShow(enrollment)}
                    ></i> */}
                    <Link to={`/enrollment/${enrollment._id}`}>
                      <i 
                        className="fa fa-pencil-square-o text-success mr-3"
                      ></i>
                    </Link>
                    <i 
                      className="fa fa-trash-o clickable text-danger" 
                      onClick={()=>this.handleDeleteShow(enrollment)} 
                    ></i>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[ 10, 25, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={enrollments.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true,
                  }}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
        <Modal
          visible={viewShow}
          onOk= {this.handleView}
          onCancel={this.handleClose}
          width={900}
        >
          <div>
            
            <h5 className="mt-3 text-center lead">student Details</h5>
            <table  className="mt-2 table table-striped table-bordered table-responsive-sm w-auto">
              <thead>
                <tr>
                  <th style={{width: "30%"}}>Name</th>
                  <th style={{width: "30%"}}>Topics</th>
                  <th style={{width: "20%"}}>enrollment Fee</th>
                  <th>enrollment Duration</th>
                  <th>Discount</th>
                </tr>
             </thead>
             <tbody>
                <tr >
                  <td>{data.name}</td>
                  <td>{data.topics}</td>
                  <td>{data.enrollment_fee}</td>
                  <td>{data.enrollment_duration}</td>
                  <td>{data.discount}</td>
                </tr>    
              </tbody>
            </table>
            
          </div>
        </Modal>
        <Modal
          visible={deleteShow}
          onOk = {this.handleDelete}
          onCancel={this.handleClose}
          okText = "Delete"
          okType = 'danger'
        >
          <p>Are you sure you want to <span className="text-danger font-bold">delete</span> this item?</p>
          <div className="text-right">
            {/* <button className="btn btn-sm btn-light mx-3" onClick={this.handleClose}>Cancel</button>
            <button className="btn btn-sm btn-danger" onClick={this.handleDelete}>Delete</button> */}

          </div>
        </Modal>
      </div>
    )
  }
}
