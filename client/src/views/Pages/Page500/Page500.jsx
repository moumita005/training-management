import React, { Component } from 'react'
import { Button, Input, InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';

export default class Page500 extends Component {
  render() {
    return (
      <div className="flex-row align-item-center"  >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-6">
              <span className='clearfix'>
                <h1 className="float-left display-3 mr-4">500</h1>
                <h4 className="pt-3">Huston We have a problem!</h4>
                <p className="text-muted float-left">The page you are looking for is temporarily unavailable</p>
              </span>
              <div className="input-group flex-nowrap">
                <div className="input-group-prepend">
                  <span className="input-group-text" id="addon-wrapping"><i className="fa fa-search"></i></span>
                </div>
                <input type="text" className="form-control" placeholder="What are you looking for?"/>
                <div className="input-group-append">
                  <button type="button" className="btn btn-outline-info">search</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
