import React, { Component } from 'react'
import semiLogo from '../../../img/semicolon.png'
import { Card, CardHeader, CardBody, CardGroup, Col, Container, Row } from 'reactstrap';
import { Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

export default class Login extends Component {
  render() {
    return (
      <div>
        <Container className="">
          <Row className="justify-content-center m-5">
            <Col md="8">
              
              <CardGroup>
                <Card className="p-3 m-4" style={{boxShadow: '5px 5px 10px '}}>
                  <CardBody className="p-5">
                    <div className="text-center ">
                      <img src={semiLogo} className="img-fluid"  />
                    </div>
                    <p className="text-center my-5">Login to your account</p>
                    <Form
                      name="login"
                      className="login-form p-5"
                      // initialValues={{ remember: true }}
                      onFinish={this.handleSubmit}
                    >
                      <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Please input your Username!' }]}
                      >
                        <Input
                          prefix={<UserOutlined className="site-form-item-icon" />}
                          placeholder="Email"
                          name="email"
                        />
                      </Form.Item>
                      <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                      >
                        <Input
                          prefix={<LockOutlined className="site-form-item-icon" />}
                          type="password"
                          placeholder="Password"
                          name="password"
                        />
                      </Form.Item>
                      

                      <Form.Item className="text-center">
                        <Button type="primary" htmlType="submit" className="my-2">
                          Log in
                        </Button>
                        {/* <br/>Or <Link to="/register">register now!</Link> */}
                      </Form.Item>
                      <div>
                        {/* {error && <p className="alert alert-danger text-center">{error}</p>} */}
                      </div>
                    </Form>
                  </CardBody>
                </Card>
                
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}
