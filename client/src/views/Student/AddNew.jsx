import React, { Component } from 'react'
import { getStudent, saveStudent } from '../../services/student';

export default class AddNewStudent extends Component {
  state = {
    data: {}
  }
  
  async componentDidMount(){
    const student_id = this.props.match.params.id
    console.log(student_id)

    if(student_id === "new") return;
    const {data: {data}} = await getStudent(student_id)
    this.setState({data})

  }

  handleChange = (e) => {
    const data = {...this.state.data};
    data[e.target.name] = e.target.value;
    this.setState({data})
  }

  handleSubmit = async(e) => {
    e.preventDefault();
    const data = {...this.state.data};
    console.log(data)
    await saveStudent(data);
    this.setState({data: {}})
  } 

  render() {
    const {data} = this.state
    return (
      <div>
        <div className="container-fluid pr-lg-5  bg-white ">
          <div className="row">
            <div className="col-md-12 ">
              <h5 className="text-center lead my-5 "><span style={{borderBottom: '2px solid'}}>Student Basic Info</span></h5>
              <form onSubmit = {this.handleSubmit}>
                <div className="row mt-5">
                  <div className="col-md-2 text-md-right">
                    <p> <span className="text-danger">*</span> Name</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="name"
                      value= {data.name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>NID</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="student_nid"
                      value= {data.student_nid || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Email</p>
                  </div>
                  <div className="col-md-4">
                    <input
                      name="email" 
                      type="email"
                      value= {data.email || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required
                      
                    />
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p> <span className="text-danger">*</span> Phone no</p>
                  </div>
                  <div className="col-md-4 ">
                    <div className="input-group mb-3 ">
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">+880</span>
                      </div>
                      <input 
                        name='phone_no'
                        value= {data.phone_no || ''}
                        className="form-control " 
                        onChange={this.handleChange} 
                        aria-label="Username" 
                        aria-describedby="basic-addon1" 
                        required
                      />
                    </div>
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col-md-2 text-md-right">
                    <p>Present Address</p>
                  </div>
                  <div className="col-md-4">
                    <div className="row">
                      <div className="col-md-6">
                        <input 
                          name="present_village"
                          value = {data.present_village || ''}
                          className="form-control form-control-sm  mb-3 " 
                          onChange={this.handleChange}
                          placeholder="present village" 
                           
                        />
                      </div>
                      <div className="col-md-6"> 
                        <input
                          name="present_post" 
                          value= {data.present_post || ''}
                          className="form-control form-control-sm  mb-3"
                          onChange={this.handleChange} 
                          placeholder="present post" 
                           
                        />
                      </div>
                    </div>
                    <div className="row ">
                      <div className="col-md-6">
                        <input 
                          name="present_upazila"
                          value= {data.present_upazila || ''}
                          className="form-control form-control-sm  my-3 " 
                          onChange={this.handleChange}
                          placeholder="present upazila" 
                           
                        />
                      </div>
                      <div className="col-md-6"> 
                        <input
                          name="present_city" 
                          value= {data.present_city || ''}
                          className="form-control form-control-sm  my-3"
                          onChange={this.handleChange} 
                          placeholder="present city" 
                           
                        />
                      </div>
                    </div>
                    <div className="row ">
                      <div className="col-md-6">
                        <input 
                          name="present_district"
                          value= {data.present_district || ''}
                          className="form-control form-control-sm  my-3" 
                          onChange={this.handleChange}
                          placeholder="present district" 
                           
                        />
                      </div>
                      <div className="col-md-6"> 
                        <input
                          name="present_division" 
                          value={data.present_division || ''}
                          className="form-control form-control-sm  my-3"
                          onChange={this.handleChange} 
                          placeholder="present division" 
                           
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>Permanent Address</p>
                  </div>
                  <div className="col-md-4">
                    <div className="row">
                      <div className="col-md-6">
                        <input 
                          name="permanent_village"
                          value= {data.permanent_village || ''}
                          className="form-control form-control-sm  mb-3 " 
                          onChange={this.handleChange}
                          placeholder="permanent village" 
                           
                        />
                      </div>
                      <div className="col-md-6"> 
                        <input
                          name="permanent_post" 
                          value={data.permanent_post || ''}
                          className="form-control form-control-sm  mb-3"
                          onChange={this.handleChange} 
                          placeholder="permanent post" 
                           
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <input 
                          name="permanent_upazila"
                          value= {data.permanent_upazila || ''}
                          className="form-control form-control-sm  my-3 " 
                          onChange={this.handleChange}
                          placeholder="permanent upazila" 
                           
                        />
                      </div>
                      <div className="col-md-6"> 
                        <input
                          name="permanent_city" 
                          value= {data.permanent_city || ''}
                          className="form-control form-control-sm  my-3"
                          onChange={this.handleChange} 
                          placeholder="permanent city" 
                           
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <input 
                          name="permanent_district"
                          value= {data.permanent_district || ''}
                          className="form-control form-control-sm  my-3 " 
                          onChange={this.handleChange}
                          placeholder="permanent district" 
                           
                        />
                      </div>
                      <div className="col-md-6"> 
                        <input
                          name="permanent_division" 
                          value = {data.permanent_division || ''}
                          className="form-control form-control-sm  my-3"
                          onChange={this.handleChange} 
                          placeholder="permanent division" 
                           
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p> <span className="text-danger">*</span>  Local Guardian Name</p>
                  </div>
                  <div className="col-md-4">
                    <input
                      name="local_guardian_name" 
                      value = {data.local_guardian_name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required
                      
                    />
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p> <span className="text-danger">*</span> Local Guardian Phone No</p>
                  </div>
                  <div className="col-md-4 ">
                    <div className="input-group mb-3 ">
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">+880</span>
                      </div>
                      <input 
                        name='local_guardian_phone_no'
                        value = {data.local_guardian_phone_no || ''}
                        className="form-control " 
                        onChange={this.handleChange} 
                        aria-describedby="basic-addon1" 
                        required
                      />
                    </div>
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p>Relation with Local Guardian</p>
                  </div>
                  <div className="col-md-4">
                    <input
                      name="relation_local_guardian" 
                      value= {data.relation_local_guardian || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      
                      
                    />
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>Local Guardian Address</p>
                  </div>
                  <div className="col-md-4 ">
                    
                    <input 
                      name='local_guardian_address'
                      value = {data.local_guardian_address || ''}
                      className="form-control form-control-sm" 
                      onChange={this.handleChange} 
                      aria-label="Username" 
                      aria-describedby="basic-addon1" 
                      
                    />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p>Local Guardian NID No</p>
                  </div>
                  <div className="col-md-4">
                    <input
                      name="local_guardian_nid_no" 
                      value = {data.local_guardian_nid_no || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      
                      
                    />
                  </div>
                  
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Description:</p>
                  </div>
                  <div className="col-md-10">
                    <textarea 
                      name="description" 
                      value={data.description || ''}
                      onChange={this.handleChange}
                      className="form-control form-control-sm " 
                      id="exampleFormControlTextarea1" rows="3"
                    ></textarea>
                  </div>
                </div>
              
              <h5 className="text-center my-5 lead"><span style={{borderBottom: '2px solid'}}>Student's Personal Info</span></h5>
              
                <div className="row mt-5">
                  <div className="col-md-2 text-md-right">
                    <p>Father's Name</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="father_name"
                      value= {data.father_name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                            
                    />
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>Mother's Name</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="mother_name"
                      value= {data.mother_name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                            
                    />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Guardian's Phone No</p>
                  </div>
                  <div className="col-md-4 ">
                    <div className="input-group mb-3 ">
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">+880</span>
                      </div>
                      <input 
                        name='guardian_phone_no'
                        value= {data.guardian_phone_no || ''}
                        className="form-control " 
                        onChange={this.handleChange} 
                        aria-label="Username" 
                        aria-describedby="basic-addon1" 
                        required
                      />
                    </div>
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>Guardian's NID No</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="guardian_nid_no"
                      value={data.guardian_nid_no || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                            
                    />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p>Gender</p>
                  </div>
                  <div className="col-md-4">
                    <select 
                      name="gender" 
                      value={data.gender || ''}
                      onChange={this.handleChange} 
                      className="form-control form-control-sm "
                    >
                      <option value="">Select</option>
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                      <option value="others">Others</option>
                    </select>
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>Marital status</p>
                  </div>
                  <div className="col-md-4">
                    <select 
                      name="marital_status" 
                      value= {data.marital_status}
                      onChange={this.handleChange} 
                      className="form-control form-control-sm "
                    >
                      <option value="">Select marital status</option>
                      <option value="single">Single</option>
                      <option value="married">Married</option>
                    </select>
                  </div>
                  
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p>Date of Birth</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="date_of_birth"
                      value={data.date_of_birth || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                            
                    />
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>Birth Certificate</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="birthcertificate"
                      value= {data.birthcertificate || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                            
                    />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Educational Qualification</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="educational_qualification"
                      value={ data.educational_qualification || '' }
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                      
                    />
                  </div>
                  <div className="col-md-2 text-md-right">
                    <p>Profession</p>
                  </div>
                  <div className="col-md-4">
                    <input 
                      name="profession"
                      value={data.profession || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                            
                    />
                  </div>
                </div>
                <button className="btn btn-outline-dark my-5 float-right" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
