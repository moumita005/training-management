import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { Modal } from 'antd'
import { deleteStudent, getStudent, getStudents } from '../../services/student';



const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };
  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
} 
export default class Students extends Component {
  state={
    students: [],
    deleteShow: false,
    viewShow:false,
    rowsPerPage:10,
    page:0,
    data: {}
  }
  
  async componentDidMount(){
    const {data:{data: data}} = await getStudents();
    console.log(data)
    this.setState({students: data});
  }

  handleViewShow = (data) => {
   this.setState({data, viewShow: true})
  }

  handleView = async () => {
    const {data} = this.state
    const student = await getStudent(data._id)
    this.setState({data: student.data.data, viewShow:false})
    
    console.log(student.data.data)
  }

  handleClose=() =>{
    this.setState({data:{}, deleteShow:false, viewShow:false})
  }
 
  handleDeleteShow = (data) => {
    this.setState({data, deleteShow: true})
  }

  handleDelete = async(e) => {
    e.preventDefault();
    const {data} = this.state;
    await deleteStudent(data._id);
    console.log(data._id)
    const students = this.state.students.filter(student=> student._id !== data._id);
    this.setState({students, deleteShow: false})


  }

  handleChangePage =(e, newPage) => {
    console.log("before",newPage)
    this.setState({page:newPage})
    console.log("after",newPage)
  }

  handleChangeRowsPerPage = (e) => {
  
    this.setState({rowsPerPage:e.target.value,page:0})
    
  }
  render() {
    const {students,page,rowsPerPage,deleteShow, viewShow,data} = this.state
    return (
      <div>
        <h2 className="text-center my-4">Student Details</h2>
        <TableContainer  component={Paper}>
          <Table  aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell className="font-weight-bold" align="left">Id</TableCell>
                <TableCell className="font-weight-bold" align="left">Name</TableCell>
                <TableCell className="font-weight-bold" align="left">Email</TableCell>
                <TableCell className="font-weight-bold" align="left">Phone no</TableCell>
                <TableCell className="font-weight-bold" align="left">Guardian Phone no</TableCell>
                <TableCell className="font-weight-bold" align="left">Educational Qualification</TableCell>
                <TableCell className="font-weight-bold" align="left">Local Guardian Phone</TableCell>
                <TableCell className="font-weight-bold" align="left">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                  ? students.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : students
                ).map((student,index) => (
                <TableRow key={index}>
                  <TableCell >{student.id} </TableCell>
                  <TableCell >{student.name} </TableCell>
                  <TableCell align="left">{student.email}</TableCell>
                  <TableCell align="left">{student.phone_no}</TableCell>
                  <TableCell align="left">{student.guardian_phone_no}</TableCell>
                  <TableCell align="left">{student.educational_qualification}</TableCell>
                  <TableCell align="left">{student.local_guardian_phone_no}</TableCell>
                  <TableCell align="left">
                    {/* <i 
                      className="fa fa-eye mr-3 text-primary" 
                      onClick={()=>this.handleViewShow(student)}
                    ></i> */}
                    <Link to={`/student/${student._id}`}>
                      <i 
                        className="fa fa-pencil-square-o text-success mr-3"
                      ></i>
                    </Link>
                    <i 
                      className="fa fa-trash-o clickable text-danger" 
                      onClick={()=>this.handleDeleteShow(student)} 
                    ></i>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[ 10, 25, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={students.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true,
                  }}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
        <Modal
          visible={viewShow}
          onOk= {this.handleView}
          onCancel={this.handleClose}
          width={900}
        >
          <div>
            
            <h5 className="mt-3 text-center lead">student Details</h5>
            <table  className="mt-2 table table-striped table-bordered table-responsive-sm w-auto">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone no</th>
                  <th>Guardian Phone no</th>
                  <th>Educational Qualification</th>
                  <th>Local Guardian Phone</th>
                </tr>
             </thead>
             <tbody>
                <tr >
                  <td>{data.name}</td>
                  <td>{data.email}</td>
                  <td>{data.phone_no}</td>
                  <td>{data.guardian_phone_no}</td>
                  <td>{data.educational_qualification}</td>
                  <td>{data.local_guardian_phone_no}</td>
                </tr>    
              </tbody>
            </table>
            
          </div>
        </Modal>
        <Modal
          visible={deleteShow}
          onOk = {this.handleDelete}
          onCancel={this.handleClose}
          okText = "Delete"
          okType = 'danger'
        >
          <p>Are you sure you want to <span className="text-danger font-bold">delete</span> this item?</p>
          <div className="text-right">
            {/* <button className="btn btn-sm btn-light mx-3" onClick={this.handleClose}>Cancel</button>
            <button className="btn btn-sm btn-danger" onClick={this.handleDelete}>Delete</button> */}

          </div>
        </Modal>
        
      </div>
    )
  }
}
