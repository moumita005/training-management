import React, { Component } from 'react'
import { getInstructor, saveInstructor } from '../../services/instructor';

export default class AddNewInstructors extends Component {
  state={
    data: {}
  }

  async componentDidMount(){
    const instructor_id = this.props.match.params.id;

    if(instructor_id === 'new') return;
    const {data:{data}} = await getInstructor(instructor_id)
    this.setState({data})
  }
  
  handleChange = (e) => {
    const data = {...this.state.data};
    data[e.target.name] = e.target.value;
    this.setState({data})
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const {data} = this.state;
    console.log(data) 
    await saveInstructor(data);
    this.setState({data: {}})
  }

  render() {
    const {data} = this.state
    return (
      <div>
        <div className="container p-lg-5  bg-white">
          <div className="row">
            <div className="col-md-12">
              <h5 className="text-center lead my-5"><span style={{borderBottom: "2px solid grey", color: '#616161!important'}}>Instructor's Information</span></h5>
              <form action="" onSubmit={this.handleSubmit}>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Name :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="name"
                      value= {data.name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>NID:</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="nid_no"
                      value= {data.nid_no || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Email :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="email"
                      value= {data.email || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                    
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Designation :</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="designation"
                      value= {data.designation || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Course:</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="course"
                      value= {data.course || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                     
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p><span className="text-danger">*</span> Phone no:</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="phone_no"
                      value= {data.phone_no || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                      required                     
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Company Name:</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="company_name"
                      value= {data.company_name || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Address:</p>
                  </div>
                  <div className="col-md-10">
                    <input 
                      name="address"
                      value= {data.address || ''}
                      className="form-control form-control-sm " 
                      onChange={this.handleChange}
                                           
                    />
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-2 text-md-right">
                    <p>Description:</p>
                  </div>
                  <div className="col-md-10">
                    <textarea 
                      name="description"
                      className="form-control" 
                      onChange= {this.handleChange}
                      rows="3"
                    ></textarea>
                  </div>
                </div>
                <button className="btn btn-outline-dark my-5 float-right" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
