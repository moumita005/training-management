
import React from "react";

const Dashboard = React.lazy(() => import('./views/Dashboard/Dashboard'))
const AddStudent = React.lazy(() => import('./views/Student/AddNew'))
const Students = React.lazy(() => import('./views/Student/Students'))
const AddInstructor = React.lazy(()=> import('./views/Instructor/AddNew'))
const Instructors = React.lazy(()=> import('./views/Instructor/Instructors'))
const AddNewCourse = React.lazy(()=> import('./views/Course/AddNew'))
const Courses = React.lazy(()=> import('./views/Course/Courses'))
const AddNewBatch = React.lazy(()=> import('./views/Batch/AddNew'))
const Batches = React.lazy(()=> import('./views/Batch/Batches'))
const AddNewEnrollment = React.lazy(()=> import('./views/Enrollment/AddNew'))
const Enrollments = React.lazy(()=>import('./views/Enrollment/Enrollments')) 
const AddNewPayment = React.lazy(()=> import('./views/Payment/AddNew'))
const Payments = React.lazy(()=>import('./views/Payment/Payments')) 

const routes = [
  { path: "/dashboard", exact: true, name: "Dashboard", component: Dashboard },
  { path: "/student/:id", exact: true, name: "Student", component: AddStudent },
  { path: "/students", exact: true, name: "Students", component: Students },
  { path: "/instructor/:id", exact: true, name: "Instructor", component: AddInstructor },
  { path: "/instructors", exact: true, name: "Instructors", component: Instructors },
  { path: "/course/:id", exact: true, name: "Course", component: AddNewCourse },
  { path: "/courses", exact: true, name: "Courses", component: Courses },
  { path: "/batch/:id", exact: true, name: "Batches", component: AddNewBatch },
  { path: "/batches", exact: true, name: "Batches", component: Batches },
  { path: "/enrollment/:id", exact: true, name: "Enrollments", component: AddNewEnrollment },
  { path: "/enrollments", exact: true, name: "Enrollments", component: Enrollments },
  { path: "/payment/:id", exact: true, name: "Payments", component: AddNewPayment },
  { path: "/payments", exact: true, name: "Enrollments", component: Payments },
]


export default routes;