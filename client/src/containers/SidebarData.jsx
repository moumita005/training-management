import React from 'react'
import {Menu} from 'antd'
import {
  AppstoreOutlined,
  BarChartOutlined,
  CloudOutlined,
  ShopOutlined,
  TeamOutlined,
  UserOutlined,
  UploadOutlined,
  VideoCameraOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  DashboardOutlined
} from '@ant-design/icons';
import {Link} from 'react-router-dom'

import data from '../_nav'

const { SubMenu } = Menu;

export default function SidebarData({isMobile, collapsed}) {

  return (
    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} className={`${isMobile && 'h-100'}`}>
      {
        data.items.map((value1, index1)=>{
          return(
            value1.children?
            (
              <SubMenu key={index1} icon={<UserOutlined />}  title={value1.title}>
                {
                  value1.children.map((value2, index2) => {
                    return(
                      <Menu.Item key={"1#"+index1+index2}><Link to={value2.url} className="text-decoration-none">{value2.name}</Link></Menu.Item>
                    )
                  })
                }
              </SubMenu>
            )
            :
            (
              <Menu.Item key={"2#"+index1} icon={value1.icon}  ><Link to={value1.url} className="text-decoration-none">{value1.name}</Link></Menu.Item>
            )
          )
        })
      }
    </Menu>
  )
}
