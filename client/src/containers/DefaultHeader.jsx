import React, { Component } from 'react'
import { Layout, Menu , Dropdown} from 'antd';
import semilogo from '../img/semicolon.png'
import {
  UserOutlined,
  MenuFoldOutlined,
  MessageOutlined,
  BellOutlined ,
} from '@ant-design/icons';


const {Header} = Layout
export default class DefaultHeader extends Component {

  handleMenuClick = e => {
    if (e.key === '3') {
      this.setState({ visible: false });
    }
  };
  render() {

    const menu = (
      <Menu onClick={this.handleMenuClick}>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="#">
            1st menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="#">
            2nd menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="#">
            3rd menu item
          </a>
        </Menu.Item>
      </Menu>
    );

    const notification = (
      <Menu onClick={this.handleMenuClick}>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="#">
            1st menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="#">
            2nd menu item
          </a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href="#">
            3rd menu item
          </a>
        </Menu.Item>
      </Menu>
    );

    const {isMobile, toggle} = this.props
    return (
      <div>
        <Header className="site-layout-background " style={{ padding: '0 20px'}}   >
          <div className='mr-3 float-left'>
            <MenuFoldOutlined  onClick={toggle} />
          </div>
            {isMobile && <img src={semilogo} />}
          <div  className='float-right mr-3'>
            
            <Dropdown 
              overlay={notification} 
              placement="bottomRight" 
              arrow
              
            >
              <BellOutlined  style={{cursor:'pointer'}} />
            </Dropdown>
              

            <MessageOutlined  className='mx-4'/>
            <Dropdown 
              overlay={menu} 
              placement="bottomRight" 
              arrow
              
            >
              <UserOutlined style={{cursor:'pointer'}} />
            </Dropdown>
          </div>
        </Header>
      </div>
    )
  }
}
