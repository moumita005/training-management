import React, {Component} from 'react'
import { Layout, Menu , Drawer, Dropdown} from 'antd';
import {
  UserOutlined,
  MenuFoldOutlined,
  MessageOutlined,
  BellOutlined ,
} from '@ant-design/icons';
import { Route, Switch } from 'react-router-dom';

import routes from '../routes';


import semilogo from '../img/semicolon.png'
import semiFavicon from '../img/Favicon.png'
import SidebarData from './SidebarData';
import DefaultHeader from './DefaultHeader';
import DefaultFooter from './DefaultFooter';

const { Content, Footer, Sider } = Layout;

const loading = () => <div className="pt-3 text-center">Loading...</div>



class DefaultLayout extends Component {
  state = {
    isMobile: false,
    collapsed: false,
    placement: 'left'
  };

  componentDidMount() {
    console.log(window.innerWidth);
    if(window.innerWidth<=600) this.setState({isMobile: true, collapsed: true});
    // this.setState()
    window.addEventListener('resize', ()=> {
      // console.log(window.innerWidth);
      this.setState({isMobile: window.innerWidth<=600, collapsed: window.innerWidth<=600});
    })
  }
  
  toggle = () => this.setState({ collapsed: !this.state.collapsed });
  
 

 

  // onClose = () =>  this.setState({ collapsed: true });

  

  render() {

    

    const { isMobile, collapsed} = this.state;
  

      return (
        <Layout className="main-layout">
          {
            isMobile ? 
            <Drawer 
              visible={!collapsed}
              placement='left'
              closable={false}
              onClose={this.toggle}
              trigger={null}
              collapsible
              title={
              <div>
                <MenuFoldOutlined className='mr-3' onClick={this.toggle} />
                <img src={semilogo} />
              </div>
            }
            >
              <SidebarData isMobile={isMobile} />
            </Drawer>  
            :
            <Sider
              className="sidebar"
              trigger={null} 
              collapsible 
              collapsed={collapsed}
              style={{
                overflow: 'auto',
                height: '100vh',
                position: 'auto',
                left: 0,
              }}
            >
              <div className="logo bg-white" style={{paddingTop:'20px',paddingBottom:'44px' }}>
                {collapsed? <img src={semiFavicon} width={26} height={26} className="mx-auto d-block" /> : <img src={semilogo} className="mx-auto d-block"  alt="" /> }
              </div>
              <SidebarData isMobile={isMobile} />
            </Sider>
          }
         
          <Layout className="site-layout">
            <DefaultHeader isMobile={isMobile} toggle={this.toggle} />
            
            
            <Content style={{ margin: '24px 16px 0'  , overflow: 'auto'  }}>
              <div className="site-layout-background " style={{ padding: 24, minHeight:'100%'}}>
                <React.Suspense fallback={loading()}>
                  <Switch>
                    {
                      routes.map((route, idx) => {
                        return route.component ? (
                          <Route 
                            key={idx} 
                            exact={route.exact} 
                            path={route.path} 
                            name={route.name} 
                            render={props=> <route.component {...props} />} 
                          />
                        ) : (null);
                      })
                    }
                  </Switch>
                </React.Suspense>   
              </div>
              <DefaultFooter />
            </Content>
        </Layout>
      </Layout>
      );
    }
}

export default DefaultLayout;

